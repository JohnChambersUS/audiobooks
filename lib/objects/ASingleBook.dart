import 'package:audiobooks/objects/DustJacket.dart';
import 'package:audiobooks/objects/TableOfContents.dart';
import 'package:flutter/material.dart';
import 'package:audiobooks/objects/Constants.dart';

//******************************************************
//* parent class for info for a single book in libary
//******************************************************
class ASingleBook {

  DustJacket dustJacket;
  TableOfContents tableOfContents;
  String identifier = "";
  ASingleBook(this.identifier) {}
  Constants c = Constants();

  //***
  //*** load dust jacket
  //***
  Future<bool> loadDustJacket() async {
    dustJacket = DustJacket(identifier);
    var result = await dustJacket.load();
    return result;
  }

  //***
  //*** load table of contents
  //***
  Future<bool> loadTableOfContents() async {
    tableOfContents = TableOfContents(identifier);
    var loaded = await tableOfContents.load();
    return loaded;
  }

  //***
  //*** write both database files
  //***
  Future<bool> writeToDatabase() async {
    var dj = await dustJacket.saveYourselfToDatabase();
    var toc = await tableOfContents.saveYourselfToDatabase();
    if (dj && toc) {
      return true;
    } else {
      return false;
    }
  }

  //***
  //*** delete both databse files
  //***
  Future<bool> deleteYourself() async {
    var bookRC = await dustJacket.deleteYourself();
    var tocRC = await tableOfContents.deleteYourself();
    return true;
  }

  //***
  //*** determine if book is complete
  //***
  bool isComplete(int chapterToPlay) {
    //determine if current episode is complete
    if (chapterToPlay == c.theNinesDecision) {
      return tableOfContents.isLastEpisodeComplete();
    } else {
      var complete = tableOfContents.isEpisdeComplete(chapterToPlay);
      if (complete) {
        tableOfContents.setNextEpisode(chapterToPlay);
        tableOfContents.resetCurrentChapterToBeginning();
      }
      return false;
    }
  }
}