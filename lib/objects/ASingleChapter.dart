import 'package:audiobooks/objects/Constants.dart';

class ASingleChapter {

  String sort = "";
  String _playPointString = "";
  int playPoint = 0;
  String _durationString = "";
  int duration = 1;
  String url = "";
  String title = "";
  String savedLength = "";

  ASingleChapter() {}

  //***
  //*** init this chapter
  //***
  bool init(String dbLine) {
    if (playPoint >= duration) {
      duration = playPoint + 10;
      _durationString = duration.toString();
    }

    if (dbLine == "") {return true;}
    var c = Constants();
    var k = 0;
    var v = 1;
    var parts = dbLine.split(c.tab);
    for(String part in parts) {
      var kv = part.split(c.separator);
      switch(kv.elementAt(k)) {
        case "sort": {sort = kv.elementAt(v);}; break;
        case "play_point": {
          _playPointString = kv.elementAt(v);
          try {playPoint = int.parse(_playPointString);}
          catch (e) {playPoint = 0;}
        }; break;
        case "duration": {
          _durationString = kv.elementAt(v);
          try {duration = int.parse(_durationString);}
          catch (e) {duration = 1;}
        }; break;
        case "url": {url = kv.elementAt(v);}; break;
        case "title": {title = kv.elementAt(v);}; break;
        case "length": {savedLength = kv.elementAt(v);}; break;
      }
    }
    return true;
  }

  resetToBeginning() {
    playPoint = 0;
    _playPointString = "0";
  }

}