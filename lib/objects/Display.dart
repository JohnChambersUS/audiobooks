import 'package:flutter/material.dart';
import 'package:audiobooks/objects/MyColors.dart';
import 'package:url_launcher/url_launcher.dart';

class Display {

  final String title = "JC4 Librivox";
  final String subTitle = "JohnChambers.us";

  final double appBarHeight = 0.08;  //0.09;

  TextStyle textStyleHeaderRegular = TextStyle (fontSize: 25.0, fontWeight: FontWeight.normal);
  TextStyle textStyleHeaderSmall = TextStyle (fontSize: 15.0, fontWeight: FontWeight.normal);

  final double flatButtonWidth = 200.0;

  MyColors colors = MyColors();

  Widget jcusWebsite;
  Widget jcusWebsiteAbout;

  Display() {
    jcusWebsite = InkWell(
      child: Text(
          "JohnChambers.us",
          style: TextStyle(
              color: colors.appBarTextColor,
              fontWeight: FontWeight.normal,
              fontSize: 15.0
          ),
      ),
      onTap: () => launch('http://johnchambers.us'),
    );

    jcusWebsiteAbout = InkWell(
      child: Text(
        "For documentation and info visit: JohnChambers.us.",
        style: TextStyle(
            color: colors.appBarTextColor,
            fontWeight: FontWeight.normal,
            fontSize: 15.0
        ),
      ),
      onTap: () => launch('http://johnchambers.us'),
    );

    textStyleHeaderRegular = TextStyle (
        color: colors.appBarTextColor,
        fontSize: 25.0,
        fontWeight: FontWeight.normal,
    );

    textStyleHeaderSmall = TextStyle (
        color: colors.appBarTextColor,
        fontSize: 15.0,
        fontWeight: FontWeight.normal
    );

  }

}