import 'package:audiobooks/objects/Constants.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class TitleSearchEntry {

  var c = Constants();

  String title = "";
  String author = "";
  String imageUrl = "";
  String thumbNailUrl = "https://archive.org/download/##id##/__ia_thumb.jpg";
  String totalTimeUrl = "https://archive.org/metadata/##id##/metadata/runtime";
  String totalTime = "0.0";
  String description = "No description available.";
  String id = "";
  String avgRating = "";
  String speed = "1.0";

  Future<String> getTotalTime() async {
    if (totalTime == "" || totalTime == null || totalTime == "0.0") {
      var rawUrl = totalTimeUrl.replaceFirst("##id##", id);
      var url = Uri.encodeFull(rawUrl);
      var response = await http.get(Uri.parse(url));
      var rc = response.statusCode;
      if (rc != 200) {
        return "";
      }
      var body = response.body;
      var j = json.decode(body);
      var t = j["result"];
      if (t == null) t = "0.0";
      totalTime = t;
      return t;
    } else {
      if (totalTime == null) totalTime = "0.0";
      return totalTime;
    }
  }

  Widget getRatingStars() {
    int stars;
    try {
      var baseStars = double.parse(avgRating);
      var base = baseStars.truncate();
      var remainder = baseStars % base;
      var tempStars = 0.0;
      if (remainder == 5) {
        tempStars = base + 0.5;
      } else {
        if (remainder > 0.5) {
          tempStars = base + 1.0;
        } else {
          tempStars = base + 0.0;
        }
      }
      stars = (tempStars * 100).toInt();
    } catch (e){
      stars = 9999;
    }
    Widget rv;
    switch (stars) {
      case 0: rv = Row(children: [Icon(Icons.star_border_rounded, color: c.display.colors.bookRowTextColor),
        Icon(Icons.star_border_rounded, color: c.display.colors.bookRowTextColor),
        Icon(Icons.star_border_rounded, color: c.display.colors.bookRowTextColor),
        Icon(Icons.star_border_rounded, color: c.display.colors.bookRowTextColor),
        Icon(Icons.star_border_rounded, color: c.display.colors.bookRowTextColor),],);
        break;
      case 50: rv = Row(children: [Icon(Icons.star_half_rounded, color: c.display.colors.bookRowTextColor),
        Icon(Icons.star_border_rounded, color: c.display.colors.bookRowTextColor),
        Icon(Icons.star_border_rounded, color: c.display.colors.bookRowTextColor),
        Icon(Icons.star_border_rounded, color: c.display.colors.bookRowTextColor),
        Icon(Icons.star_border_rounded, color: c.display.colors.bookRowTextColor),],);
        break;
      case 100: rv = Row(children: [Icon(Icons.star_rounded, color: c.display.colors.bookRowTextColor),
        Icon(Icons.star_border_rounded, color: c.display.colors.bookRowTextColor),
        Icon(Icons.star_border_rounded, color: c.display.colors.bookRowTextColor),
        Icon(Icons.star_border_rounded, color: c.display.colors.bookRowTextColor),
        Icon(Icons.star_border_rounded, color: c.display.colors.bookRowTextColor),],);
        break;
      case 150: rv = Row(children: [Icon(Icons.star_rounded, color: c.display.colors.bookRowTextColor),
        Icon(Icons.star_half_rounded, color: c.display.colors.bookRowTextColor),
        Icon(Icons.star_border_rounded, color: c.display.colors.bookRowTextColor),
        Icon(Icons.star_border_rounded, color: c.display.colors.bookRowTextColor),
        Icon(Icons.star_border_rounded, color: c.display.colors.bookRowTextColor),],);
        break;
      case 200: rv = Row(children: [Icon(Icons.star_rounded, color: c.display.colors.bookRowTextColor),
        Icon(Icons.star_rounded, color: c.display.colors.bookRowTextColor),
        Icon(Icons.star_border_rounded, color: c.display.colors.bookRowTextColor),
        Icon(Icons.star_border_rounded, color: c.display.colors.bookRowTextColor),
        Icon(Icons.star_border_rounded, color: c.display.colors.bookRowTextColor),],);
        break;
      case 250: rv = Row(children: [Icon(Icons.star_rounded, color: c.display.colors.bookRowTextColor),
        Icon(Icons.star_rounded, color: c.display.colors.bookRowTextColor),
        Icon(Icons.star_half_rounded, color: c.display.colors.bookRowTextColor),
        Icon(Icons.star_border_rounded, color: c.display.colors.bookRowTextColor),
        Icon(Icons.star_border_rounded, color: c.display.colors.bookRowTextColor),],);
        break;
      case 300: rv = Row(children: [Icon(Icons.star_rounded, color: c.display.colors.bookRowTextColor),
        Icon(Icons.star_rounded, color: c.display.colors.bookRowTextColor),
        Icon(Icons.star_rounded, color: c.display.colors.bookRowTextColor),
        Icon(Icons.star_border_rounded, color: c.display.colors.bookRowTextColor),
        Icon(Icons.star_border_rounded, color: c.display.colors.bookRowTextColor),],);
        break;
      case 350: rv = Row(children: [Icon(Icons.star_rounded, color: c.display.colors.bookRowTextColor),
        Icon(Icons.star_rounded, color: c.display.colors.bookRowTextColor),
        Icon(Icons.star_rounded, color: c.display.colors.bookRowTextColor),
        Icon(Icons.star_half_rounded, color: c.display.colors.bookRowTextColor),
        Icon(Icons.star_border_rounded, color: c.display.colors.bookRowTextColor),],);
        break;
      case 400: rv = Row(children: [Icon(Icons.star_rounded, color: c.display.colors.bookRowTextColor),
        Icon(Icons.star_rounded, color: c.display.colors.bookRowTextColor),
        Icon(Icons.star_rounded, color: c.display.colors.bookRowTextColor),
        Icon(Icons.star_rounded, color: c.display.colors.bookRowTextColor),
        Icon(Icons.star_border_rounded, color: c.display.colors.bookRowTextColor),],);
        break;
      case 450: rv = Row(children: [Icon(Icons.star_rounded, color: c.display.colors.bookRowTextColor),
        Icon(Icons.star_rounded, color: c.display.colors.bookRowTextColor),
        Icon(Icons.star_rounded, color: c.display.colors.bookRowTextColor),
        Icon(Icons.star_rounded, color: c.display.colors.bookRowTextColor),
        Icon(Icons.star_half_rounded, color: c.display.colors.bookRowTextColor),],);
        break;
      case 500: rv = Row(children: [Icon(Icons.star_rounded, color: c.display.colors.bookRowTextColor),
        Icon(Icons.star_rounded, color: c.display.colors.bookRowTextColor),
        Icon(Icons.star_rounded, color: c.display.colors.bookRowTextColor),
        Icon(Icons.star_rounded, color: c.display.colors.bookRowTextColor),
        Icon(Icons.star_rounded, color: c.display.colors.bookRowTextColor),],);
        break;
      break;
      default: rv = Row(children: [],);
        break;
    }
    return rv;

  }

  void setId(String idValue) {
    id = idValue;
    //set thumbNailUrl
    thumbNailUrl = thumbNailUrl.replaceFirst("##id##", id);
    imageUrl = thumbNailUrl;
  }

  void setTitle(String titleValue) {
    title = titleValue;
  }

  void addAuthor(String authorValue) {
    authorValue = authorValue.trim();
    if (author == "") {
      author = authorValue;
    } else {
      author = author + ", " + authorValue;
    }
  }

}