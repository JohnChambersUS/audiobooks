import 'dart:io';
import 'package:path_provider/path_provider.dart';
import 'package:audiobooks/objects/Constants.dart';
import 'dart:io';
import 'package:audiobooks/objects/ASingleChapter.dart';

class TableOfContents {

  Constants c = Constants();

  List<ASingleChapter> chapters = [];
  String _identifier = "";
  String _urlTemplate = "##id##.table_of_contents.dbfile";
  int _currentEpisode = 0;
  TableOfContents(this._identifier) {}

  //*********************************************
  //*********************************************
  //* playlist control
  //*********************************************
  //*********************************************

  //***
  //*** set next episode
  //***
  void setNextEpisode(int episodeToPlay) {
    _currentEpisode = 0;
    if (episodeToPlay == c.theNinesDecision) {
      _currentEpisode = findNextEpisode();
    } else {
      _currentEpisode = verifySpecifiedEpisode(episodeToPlay);
    }
  }

  int verifySpecifiedEpisode(int specifiedEpisode) {
    if (specifiedEpisode >= 0) {
      if (specifiedEpisode > chapters.length) {
        return 0;
      } else {
        if (chapters[specifiedEpisode].playPoint == chapters[specifiedEpisode].duration) {
          chapters[specifiedEpisode].resetToBeginning();
        }
        return specifiedEpisode;
      }
    } else {
      return 0;
    }
  }

  int findNextEpisode() {
    var i = chapters.length - 1;
    while (i > -1) {
      if (chapters.elementAt(i).playPoint > 0) {
        if (chapters.elementAt(i).playPoint < chapters.elementAt(i).duration) {
          return i;
        } else {
          //move forward 1;
          i++;
          return i;
        }
      }
      i--;
    }
    return 0;
  }

  //***
  //*** get current url
  //***
  String getCurrentUrl() {
    if (_currentEpisode < chapters.length) {
      return chapters.elementAt(_currentEpisode).url;
    }
    else {
      return "";
    }
  }

  //***
  //*** set next counter
  //***
  bool incrementEpisode() {
    _currentEpisode++;
    if (_currentEpisode >= chapters.length) {
      return false;
    } else {
      return true;
    }
  }

  //******************************************
  //******************************************
  //* data getters
  //******************************************
  //******************************************

  //***
  //*** get current title
  //***
  String getCurrentTitle() {
    try {
      return chapters.elementAt(_currentEpisode).title;
    } catch (e) {
      return "";
    }
  }

  //***
  //*** get play point
  //***
  int getPlayPoint() {
    try {
      if (_currentEpisode >= 0 && _currentEpisode < chapters.length) {
        var p = chapters.elementAt(_currentEpisode).playPoint;
        return chapters.elementAt(_currentEpisode).playPoint;
      }
    } catch (e) {
     var x = e;
    }
   }

  //***
  //*** get play point
  //***
  int getDuration() {
    if (_currentEpisode >= 0 && _currentEpisode < chapters.length) {
      return chapters.elementAt(_currentEpisode).duration;
    }
  }

  //*******************************************
  //*******************************************
  //* data setters
  //*******************************************
  //*******************************************

  //***
  //*** update play point in current chapter
  //***
  void updatePlayPoint(int playPoint) {
    if (_currentEpisode >= 0 && _currentEpisode < chapters.length) {
      chapters.elementAt(_currentEpisode).playPoint = playPoint;
    }
  }

  //***
  //*** update duration
  //***
  void updateDuration(int duration) {
    if (_currentEpisode >= 0 && _currentEpisode < chapters.length) {
      chapters.elementAt(_currentEpisode).duration = duration;
    }
  }

  //***
  //*** reset all episodes to beginning
  //***
  bool resetAllEpisodesToBeginning() {
    for (ASingleChapter chapter in chapters) {
      chapter.resetToBeginning();
      //chapter.playPoint = 0;
      var x = 1;
    }
    _currentEpisode = 0;
    return true;
  }

  //***
  //*** reset current chapter to beginning
  //***
  bool resetCurrentChapterToBeginning() {
    //chapters.elementAt(_currentEpisode).playPoint = 0;
    if (_currentEpisode >= 0 && _currentEpisode < chapters.length) {
      chapters.elementAt(_currentEpisode).resetToBeginning();
    }
    return true;
  }

  //*****************************************
  //*****************************************
  //* load table of contents from disk
  //*****************************************
  //*****************************************
  Future<bool> load() async {
    final directory = await getApplicationDocumentsDirectory();
    var path = directory.path.toString() + "/" + _urlTemplate;
    path = path.replaceFirst("##id##", _identifier);
    var file = File(path);
    if (await file.exists()) {
     var contents = await file.readAsString();
     return await parseFileContents(contents);
    } else {
     return false;
    }
  }
  //***
  //*** partner routing to load()
  //***
  Future<bool> parseFileContents(String contents) async {
    var c = Constants();
    var lines = contents.split(c.newLine);
    for (String line in lines) {
     if (line.length > 0) {
      var chapter = ASingleChapter();
      chapter.init(line);
      chapters.add(chapter);
     }
    }
    return true;
  }

  //**************************************************
  //**************************************************
  //* write table of contents file
  //**************************************************
  //**************************************************
  Future<bool> saveYourselfToDatabase() async {
    var os = "";
    for (ASingleChapter chapter in chapters) {
      os = apppendLineToOutputString(os, chapter);
    }
    // trim last new line character
    os = os.substring(0, os.length - 1);

    var saveFileName = _urlTemplate.replaceFirst("##id##",_identifier);

    final directory = await getApplicationDocumentsDirectory();
    var path = directory.path.toString() + "/" + saveFileName;
    var f = File(path);
    var out;
    try {
      out = await f.writeAsString(os);
    } catch (e) {
      return false;
    }
    return true;
  }

  //***
  //*** append line to output string
  //***
  String apppendLineToOutputString(String os, ASingleChapter chapter) {
    var c = Constants();
    os += "sort" + c.separator + chapter.sort + c.tab;
    os += "play_point" + c.separator + chapter.playPoint.toString() + c.tab;
    os += "duration" + c.separator + chapter.duration.toString() + c.tab;
    os += "url" + c.separator + chapter.url + c.tab;
    os += "title" + c.separator + chapter.title + c.newLine;

    return os;
  }

  //**********************************************
  //**********************************************
  //* delete this file
  //**********************************************
  //**********************************************
  Future<bool> deleteYourself() async {
    var saveFileName = _urlTemplate.replaceFirst("##id##",_identifier);

    final directory = await getApplicationDocumentsDirectory();
    var path = directory.path.toString() + "/" + saveFileName;
    var file = File(path);
    if (await file.exists()) {
      var rc = await file.delete(recursive: false);
      if (rc == 1) {
        return true;
      } else {
        return false;
      }
    } else {
      return true;
    }
  }

  bool isEpisdeComplete(int episodeToPlay) {
    try {
      _currentEpisode = episodeToPlay;
      if (getPlayPoint() >= getDuration())
        return true;
      else
        return false;
    } catch (e) {
      return true;
    }
  }

  bool isLastEpisodeComplete() {
    var lastEpisode = chapters.length - 1;
    if (chapters.elementAt(lastEpisode).playPoint >= chapters.elementAt(lastEpisode).duration) {
      return true;
    }
    return false;
  }

 }