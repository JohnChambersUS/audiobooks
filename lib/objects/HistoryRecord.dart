
class HistoryRecord {

  String title = "";
  String author = "";
  String imageUrl = "";
  String description = "";
  String identifier = "";
  double avgRating = 0.0;
  String totalTime = "";
  String addDate = "";

  String _t = "\t";
  String _n = "\n";

  HistoryRecord(String record) {

    List<String> records = record.split("\t");
    title = records.elementAt(0);
    author = records.elementAt(1);
    imageUrl = records.elementAt(2);
    description = records.elementAt(3);
    identifier = records.elementAt(4);
    avgRating = double.parse(records.elementAt(5));
    totalTime = records.elementAt(6);
    addDate = records.elementAt(7);

  }

  String convertToString() {
    String r = "";
    r += title + _t +
        author + _t +
        imageUrl + _t +
        description + _t +
        identifier + _t +
        avgRating.toString() + _t +
        totalTime + _t +
        addDate + _n;
    return r;
  }
}