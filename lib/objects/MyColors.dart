
import 'package:flutter/material.dart';

class MyColors{

  var theme = "purple2";

  Color appBarColor;
  Color appBarTextColor;
  //Color trimColor;

  Color buttonBorderColor = Colors.black87;
  Color buttonTextColor = Colors.black87;
  Color flatButtonColor = Colors.white;

  Color searchBarColor = Colors.white;
  Color bookRowColor = Colors.tealAccent;
  Color bookRowTextColor = Colors.white;
  Color bookCoverBackgroundColor = Colors.black87;
  Color playerBackgroundColor = Colors.black87;
  Color menuBarColor = Colors.black12;
  Color menuBarIconColor = Colors.white;
  Color playerTitleColor = Colors.white;
  Color sliderInactiveColor = Colors.grey;

  Color circleColor = Colors.redAccent;

  Color inputBackgroundColor = Colors.white;
  Color inputTextColor = Colors.black;
  Color inputBorderColor = Colors.black;

  Color backgroundColor1 = Colors.black;
  Color backgroundColor2 = Colors.black;

  Color mainBackgroundColor1 = Colors.black;
  Color mainBackgroundColor2 = Colors.black;

  Color mainPageMenuColor = Colors.green;

  String background = "";



  MyColors() {
    setColors();
  }

  MaterialColor mapColor(String hexString) {

    var r = int.parse(hexString.substring(4,6), radix: 16);
    var g = int.parse(hexString.substring(6,8), radix: 16);
    var b = int.parse(hexString.substring(8,10), radix: 16);

    Map<int, Color> colorMap =
    {
       50: Color.fromRGBO(r, g, b, .1),
      100: Color.fromRGBO(r, g, b, .2),
      200: Color.fromRGBO(r, g, b, .3),
      300: Color.fromRGBO(r, g, b, .4),
      400: Color.fromRGBO(r, g, b, .5),
      500: Color.fromRGBO(r, g, b, .6),
      600: Color.fromRGBO(r, g, b, .7),
      700: Color.fromRGBO(r, g, b, .8),
      800: Color.fromRGBO(r, g, b, .9),
      900: Color.fromRGBO(r, g, b, 1),
    };
    return MaterialColor(int.parse(hexString), colorMap);
  }



  void setColors() {

    switch(theme) {
      case "dark15": {
        appBarColor = mapColor("0xff46536f");
        appBarTextColor = mapColor("0xffe7e7e7");
        menuBarColor = mapColor("0xffe7e7e7");

        bookCoverBackgroundColor = mapColor("0xff333335");

        playerBackgroundColor = mapColor("0xff333335");

        bookRowColor = mapColor("0xfffafafa");
        bookRowTextColor = mapColor("0xff333335");

        circleColor = mapColor("0xffd4292a");

        menuBarIconColor = mapColor("0xff333335");
        playerTitleColor = mapColor("0xffFafafa");
        sliderInactiveColor = mapColor("0xffcccccc");
        searchBarColor = mapColor("0xffFFFFFF");

        background = "assets/backgroundslate.jpg";
      }
      break;

      //******************************
      //* purple
      //******************************
      case "purple": {
        //** main color
        //appBarColor = mapColor("0xff46536f");
        appBarColor = mapColor("0xff4d2899");
        appBarTextColor = mapColor("0xffffffff");
        menuBarColor = mapColor("0xffe7e7e7");

        backgroundColor1 = mapColor("0xff361c5a");
        backgroundColor2 = mapColor("0xff866de1");

        bookCoverBackgroundColor = mapColor("0xff080512");

        //** main color
        playerBackgroundColor = mapColor("0xff080512");
        //playerBackgroundColor = mapColor("0xff1A3315");

        //bookRowColor = mapColor("0xff333335");
        bookRowColor = mapColor("0xff080512");
        bookRowTextColor = mapColor("0xffe7e7e7");

        //circleColor = mapColor("0xffd4292a");
        circleColor = mapColor("0xff866de1");

        menuBarIconColor = mapColor("0xff080512");
        playerTitleColor = mapColor("0xffFafafa");
        sliderInactiveColor = mapColor("0xffcccccc");
        searchBarColor = mapColor("0xffFFFFFF");

        inputBackgroundColor = mapColor("0xffFFFFFF");
        inputTextColor = mapColor("0xff080512");
        inputBorderColor = mapColor("0xff080512");

        background = "assets/backgroundslate.jpg";
      }
      break;

    //******************************
    //* purple2
    //******************************
      case "purple2": {
        //** main color
        //appBarColor = mapColor("0xff46536f");
        appBarColor = mapColor("0xff4d2899");
        appBarTextColor = mapColor("0xffffffff");
        menuBarColor = mapColor("0xffe7e7e7");

        mainPageMenuColor = mapColor("0xffe7e7e7");

        //backgroundColor1 = mapColor("0xff361c5a");
        //backgroundColor2 = mapColor("0xff866de1");

        backgroundColor1 = mapColor("0xff0b0c0b");
        backgroundColor2 = mapColor("0xff0b0c0b");

        mainBackgroundColor1 = mapColor("0xff0b0c0b");
        mainBackgroundColor2 = mapColor("0xff0b0c0b");

        bookCoverBackgroundColor = mapColor("0xff0b0c0b");

        //** main color
        playerBackgroundColor = mapColor("0xff0b0c0b");
        //playerBackgroundColor = mapColor("0xff1A3315");

        //bookRowColor = mapColor("0xff333335");
        bookRowColor = mapColor("0xff0b0c0b");
        bookRowTextColor = mapColor("0xffe7e7e7");

        //circleColor = mapColor("0xffd4292a");
        circleColor = mapColor("0xff866de1");

        menuBarIconColor = mapColor("0xff080512");
        playerTitleColor = mapColor("0xffFafafa");
        sliderInactiveColor = mapColor("0xffcccccc");
        searchBarColor = mapColor("0xffFFFFFF");

        inputBackgroundColor = mapColor("0xffFFFFFF");
        inputTextColor = mapColor("0xff080512");
        inputBorderColor = mapColor("0xff080512");

        background = "assets/backgroundslate.jpg";
      }
      break;

      default: {
        appBarColor = mapColor("0xff71814e");
        bookRowColor = mapColor("0xffFFF8E6");
        searchBarColor = mapColor("0xffFFFFFF");
        bookCoverBackgroundColor = mapColor("0xff4C4637");
        menuBarColor = mapColor("0xff71814e");
        menuBarIconColor = mapColor("0xffffffff");
        playerTitleColor = mapColor("0xffffffff");
        sliderInactiveColor = mapColor("0xffcccccc");
        circleColor = mapColor("0xffe43841");
        playerBackgroundColor = mapColor("0xff4C4637");
        background = "assets/backgroundslate.jpg";
      }
      break;

    }

  }

}