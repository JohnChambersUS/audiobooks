
class EpisodeEntry {

  var _fileName = "";
  var episodeName = "";
  var _identity = "";
  var _imageName = "";
  var _length = "";
  
  var _downloadPath = "https://archive.org/download/##id##/##file##";

  void setLength(String length) {
    _length = length;
  }

  String getLength() {
    return _length;
  }

  void setFileName(String name) {
    _fileName = name;
  }

  void setEpisodeName(String name) {
    episodeName = name;
  }

  void setIdentity(String identity) {
    _identity = identity;
  }

  String getIdentity() {
    return _identity;
  }

  void setImageName(String imageName) {
    _imageName = imageName;
  }

  String getUrl() {
    var url = _downloadPath.replaceFirst("##id##", _identity);
    url = url.replaceFirst("##file##", _fileName);
    return url;
  }

  String getImagePath() {
    var url = _downloadPath.replaceFirst("##id##", _identity);
    url = url.replaceFirst("##file##", _imageName);
    return url;
  }

  String getEpisodeTitle() {
    return episodeName;
  }

}