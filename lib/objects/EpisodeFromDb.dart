

class EpisodeFromDb {

  String sort = "";
  String playPoint = "";
  String duration = "";
  String url = "";
  String title = "";
  String speed = "";

  int getPlayPointAsInt() {
    return int.parse(playPoint);
  }

  int getDurationAsInt() {
    return int.parse(duration);
  }

  double getSpeedAsDouble() {
    double.parse(speed);
  }

}