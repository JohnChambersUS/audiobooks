import 'package:audiobooks/objects/ASingleBook.dart';
import 'package:path_provider/path_provider.dart';

class Library {

  List<ASingleBook> books = [];

  //***********************************
  //* load book list from file
  //***********************************
  Future<bool> loadBooks() async {
    final directory = await getApplicationDocumentsDirectory();
    final files = await directory.list().toList();
    for (final fileName in files) {
      var sections = fileName.path.toString().split("/");
      final parts = sections.last.split(".");
      final type = parts.last.trim();
      if (type == "dbfile") {
        if (parts.elementAt(parts.length -2) == "dust_jacket") {
          //build id from multiple parts
          parts.removeAt(parts.length - 1);
          parts.removeAt(parts.length - 1);
          var id = parts.join(".");
          var book = ASingleBook(id);
          var rc = await book.loadDustJacket();
          if (rc) {
            books.add(book);
          }
        }
      }
    }
    return true;
  }

}