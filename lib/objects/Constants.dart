
import 'package:audiobooks/objects/Display.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'dart:math';


class Constants {

  static Constants _instance;

  String newestFirst = "Newest First";
  String anyCategory = "Any Category";
  String tab = "\t";
  String newLine = "\n";
  String separator = ":©®:";

  int theNinesDecision = 9999999;

  var display = Display();

  String applicationPath;

  List<String> categories =
      ["Any Category", "adventure", "animals", "autobiography", "biography", "children", "christmas", "comedy", "cooking",
      "crime", "detective", "drama", "essays", "fables", "fairy tales", "family", "fantasy", "fiction", "historical fiction",
      "history", "humor", "insects", "instruction", "legends", "literature", "love", "memoirs", "music", "mystery", "myths",
      "nature", "non-fiction", "nonfiction", "philosophy", "poetry", "psychology", "religion", "romance", "satire", "science",
      "science fiction", "short stories", "theology", "tragedy", "travel", "war", "western", "young adult", "youth"];

  List<String> backgroundImages = ["assets/main_background_cows.jpg",
    "assets/main_background_turtles1.jpg",
    "assets/main_background_cats.jpg",
    "assets/main_background_bears.jpg",
    "assets/main_background_snakes.jpg",
    "assets/main_background_mice.jpg",
    "assets/main_background_turtles2.jpg",];

    Constants._internal() {

    _instance = this;
  }

  factory Constants() {
    return  _instance ?? Constants._internal();
  }
  
}