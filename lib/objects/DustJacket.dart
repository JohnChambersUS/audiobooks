import 'dart:io';
import 'package:path_provider/path_provider.dart';
import 'package:audiobooks/objects/Constants.dart';

//*******************************************
//* this class contians the summary data
//* about a book--a meta data file
//*******************************************
class DustJacket {

  String title = "";
  String author = "";
  String imageUrl = "";
  String description = "";
  String identifier = "";
  String avgRatingString = "";
  double avgRating = 0.0;
  String speedString = "";
  double speed = 1.0;

  String _urlTemplate = "##id##.dust_jacket.dbfile";

  DustJacket(this.identifier) {}

  //***
  //*** load the dustjacket
  //***
  Future<bool> load() async {
    final directory = await getApplicationDocumentsDirectory();
    var path = directory.path.toString() + "/" + _urlTemplate;
    path = path.replaceFirst("##id##", identifier);
    var file = File(path);
    if (await file.exists()) {
      var contents = await file.readAsString();
      return parseFileContents(contents);
    } else {
      return false;
    }
  }

  //***
  //*** delete the dust jacket
  //***
  Future<bool> deleteYourself() async {
    var saveFileName = _urlTemplate.replaceFirst("##id##",identifier);

    final directory = await getApplicationDocumentsDirectory();
    var path = directory.path.toString() + "/" + saveFileName;
    var file = File(path);
    if (await file.exists()) {
      var rc = await file.delete(recursive: false);
      if (rc == 1) {
        return true;
      } else {
        return false;
      }
    } else {
      return true;
    }
  }

  //***
  //*** parse the contents of dustjacket load
  //***
  bool parseFileContents(String contents) {
    var c = Constants();
    var k = 0;
    var v = 1;
    var lines = contents.split(c.newLine);
    for (String line in lines) {
      var kv = line.split(c.separator);
      if (kv.length > 1) {
        switch(kv.elementAt(k)) {
          case "title": {title = kv.elementAt(v);} break;
          case "author": {author = kv.elementAt(v);} break;
          case "image_url": {imageUrl = kv.elementAt(v);} break;
          case "description": {description = kv.elementAt(v);} break;
          case "identifier": {identifier = kv.elementAt(v);} break;
          case "avg_rating": {
            avgRatingString = kv.elementAt(v);
            try {
              avgRating = double.parse(avgRatingString);
            } catch (e) {
              avgRating = 0.0;
            }
          } break;
          case "speed": {
            speedString = kv.elementAt(v);
            try {
              speed = double.parse(speedString);
            } catch (e) {
              speed = 1.0;
            }
          } break;
        }
      }
    }
    return true;
  }

  //*******************************************
  //*******************************************
  //* write the contents of the dust jacket
  //*******************************************
  //*******************************************
  Future<bool> saveYourselfToDatabase() async {
    var c = Constants();
    var os = "";
    os += "title" + c.separator + title + c.newLine;
    os += "author" + c.separator + author + c.newLine;
    os += "image_url" + c.separator + imageUrl + c.newLine;
    os += "description" + c.separator + description + c.newLine;
    os += "identifier" + c.separator + identifier + c.newLine;
    os += "avg_rating" + c.separator +  avgRating.toString() + c.newLine;
    os += "speed" + c.separator + speed.toString();

    var saveFileName = _urlTemplate.replaceFirst("##id##",identifier);

    final directory = await getApplicationDocumentsDirectory();
    var path = directory.path.toString() + "/" + saveFileName;
    var f = File(path);
    var rc;
    try {
      rc = await f.writeAsString(os);
    } catch (e) {
      return false;
    }
    return true;
  }

}