
import 'package:audiobooks/modules/ASearchTitle.dart';
import 'package:audiobooks/screens/SearchResultScreen.dart';
import 'package:flutter/material.dart';
import 'package:audiobooks/objects/Constants.dart';

class TitleSearchScreen extends StatefulWidget {
  @override
  _TitleSearchScreenState createState() => _TitleSearchScreenState();
}

class _TitleSearchScreenState extends State<TitleSearchScreen> {

  var c = Constants();

  final _titleFormKey = GlobalKey<FormState>();
  var _titleDropDownValue = "Contains";
  var _titleCategoryValue = "Any Category";
  var _titleTextController = TextEditingController();

  //***
  //*** main override
  //***
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        actions: [
          IconButton(
              icon: Icon(Icons.home),
              onPressed: () {
                Navigator.of(context).pop('home');
              }),
        ],
        title: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(c.display.title, style: c.display.textStyleHeaderRegular),
                //Text(c.display.subTitle, style: c.display.textStyleHeaderSmall),
                //c.display.jcusWebsite,
              ],
            ),
          ),
        ),
        toolbarHeight: MediaQuery.of(context).size.height * c.display.appBarHeight,
      ),

      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage(c.backgroundImages.elementAt(2)),
              fit: BoxFit.fitWidth,
              alignment: Alignment.bottomCenter,
            ),
          gradient: LinearGradient(
            colors: [
              c.display.colors.backgroundColor1,
              c.display.colors.backgroundColor2,
              c.display.colors.backgroundColor1,
              c.display.colors.backgroundColor2
            ],
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
          ),
        ),

        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            //**********************************
            //* search form
            //***********************************

            Padding(
              padding: const EdgeInsets.only(bottom: 0.0, top: 0.0, left: 0.0, right: 0.0),
              child: Container(
                  child: Form(
                      key: _titleFormKey,
                      child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            //*** title ***
                            Padding(
                              padding: const EdgeInsets.only(left: 8.0, right: 8.0, bottom: 8.0, top: 12.0),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  Text("Title Search",
                                      style: TextStyle(
                                        fontSize: 24.0,
                                        color: c.display.colors.inputBackgroundColor,
                                      )),
                                  Padding(
                                    padding: const EdgeInsets.only(left: 16, top: 12, right: 8, bottom: 8),
                                    child: Icon(
                                      Icons.title,
                                      color: c.display.colors.mainPageMenuColor,
                                    ),
                                  ),
                                ],
                              ),
                            ),

                            //*** input box ***
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Container(
                                child: TextFormField(
                                  controller: _titleTextController,
                                  decoration: InputDecoration(
                                    hintText: "Enter a Title",
                                    border: OutlineInputBorder(borderRadius: BorderRadius.circular(9.0), gapPadding: 8.0,  ) ,
                                    contentPadding: EdgeInsets.only(left: 12.0, right: 12.0, bottom: 12.0, top: 12.0),
                                    fillColor: c.display.colors.inputBackgroundColor,
                                    filled: true,
                                  ),
                                  validator: (value) {
                                    if (value.isEmpty) {
                                      return 'Please enter some text';
                                    }
                                    return null;
                                  },
                                ),
                              ),
                            ),

                            //*** drop down for position in string
                            Padding(
                              padding: const EdgeInsets.only(left: 0.0, right: 0.0, top: 0.0, bottom: 32.0),
                              child: Row(
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: getTitleCategoryDropdown(),
                                  ),

                                  Spacer(),

                                  //*** icon button to perform search
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Container(
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(9.0),
                                        border: Border.all(color: c.display.colors.inputBorderColor),
                                        color: c.display.colors.inputBackgroundColor,
                                      ),
                                      child: IconButton(
                                        icon: Icon(Icons.search_rounded, size: 25),
                                        onPressed: () {
                                          if ( _titleTextController.value.text.length > 0) {
                                            FocusScopeNode currentFocus = FocusScope.of(context);
                                            if (!currentFocus.hasPrimaryFocus) {
                                              currentFocus.unfocus();
                                            }
                                            var theSearch = ASearchTitle();
                                            theSearch.setTitleToSearch(_titleTextController.value.text);
                                            theSearch.setCategoryToSearch(_titleCategoryValue);
                                            Navigator.push(context, MaterialPageRoute(builder: (context) => SearchResultScreen(theSearch)))
                                                .then((value){
                                              if (value == 'home') {
                                                Navigator.of(context).pop('home');
                                              }
                                            });
                                          }
                                        },
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ]
                      )
                  )
              ),
            ),
          ],
        ),
      ), //end of body container
    );
  } //end of build

//************************************************
//* begining of functions
//************************************************

  //***
  //*** get title category dropdown
  //***
  Widget getTitleCategoryDropdown() {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(9.0),
        border: Border.all(color: c.display.colors.inputBorderColor),
        color: c.display.colors.inputBackgroundColor,
      ),
      //color: Colors.white,
      child: Padding(
        padding: const EdgeInsets.only (top: 0.0, bottom: 0.0, left: 8.0, right: 8.0),
        child: DropdownButtonHideUnderline(
          child: DropdownButton<String>(
            dropdownColor: c.display.colors.inputBackgroundColor,
            value: _titleCategoryValue,
            icon: Icon(Icons.arrow_drop_down_rounded),
            iconSize: 24,
            elevation: 16,
            style: TextStyle(color: c.display.colors.inputTextColor),
            onChanged: (String newValue) {
              setState(() {
                _titleCategoryValue = newValue;
              });
            },
            items: c.categories
                .map<DropdownMenuItem<String>>((String value) {
              return DropdownMenuItem<String>(
                value: value,
                child: Text(value),
              );
            }).toList(),
          ),
        ),
      ),
    );
  }
}
