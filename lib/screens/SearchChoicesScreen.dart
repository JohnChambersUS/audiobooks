
import 'package:audiobooks/modules/ASearchCategory.dart';
import 'package:audiobooks/screens/SearchResultScreen.dart';
import 'package:audiobooks/screens/TitleSearchScreen.dart';
import 'package:audiobooks/screens/AuthorSearchScreen.dart';
import 'package:audiobooks/screens/CategorySearchScreen.dart';
import 'package:audiobooks/screens/NarratorSeachScreen.dart';
import 'package:flutter/material.dart';
import 'package:audiobooks/objects/Constants.dart';

class SearchChoicesScreen extends StatefulWidget {
  @override
  _SearchChoicesScreenState createState() => _SearchChoicesScreenState();
}

class _SearchChoicesScreenState extends State<SearchChoicesScreen> {

  var c = Constants();

  //***
  //*** main override
  //***
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: [
          IconButton(
              icon: Icon(Icons.home),
              onPressed: () {
                Navigator.of(context).pop('home');
              }),
        ],
        title: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(c.display.title, style: c.display.textStyleHeaderRegular),
                //Text(c.display.subTitle, style: c.display.textStyleHeaderSmall),
                //c.display.jcusWebsite,
              ],
            ),
          ),
        ),
        toolbarHeight: MediaQuery.of(context).size.height * c.display.appBarHeight,
      ),

      body: Container(
        decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage(c.backgroundImages.elementAt(1)),
              fit: BoxFit.fitWidth,
              alignment: Alignment.bottomCenter,
            ),
            gradient: LinearGradient(
              colors: [
                c.display.colors.backgroundColor1,
                c.display.colors.backgroundColor2,
                c.display.colors.backgroundColor1,
                c.display.colors.backgroundColor2
              ],
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
            ),
        ),

        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            //***
            //*** title search textbutton
            //***
            TextButton(
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => TitleSearchScreen())
                ).then((value) {
                  if (value == 'home') {
                    Navigator.of(context).pop('home');
                  }
                });
              },
              child: Padding(
                padding: const EdgeInsets.only(top: 12, right: 8, bottom: 4),
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Text("Title Search",
                        style: TextStyle(
                          color: c.display.colors.mainPageMenuColor,
                          fontSize: 24,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 16, top: 8, right: 8, bottom: 4),
                        child: Icon(
                          Icons.title,
                          color: c.display.colors.mainPageMenuColor,
                        ),
                      ),
                    ]),
              ),
            ), //end of title textbutton

            //***
            //*** author search textbutton
            //***
            TextButton(
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => AuthorSearchScreen())
                ).then((value) {
                  if (value == 'home') {
                    Navigator.of(context).pop('home');
                  }
                });
              },
              child: Padding(
                padding: const EdgeInsets.only(top: 4, right: 8, bottom: 4),
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Text("Author Search",
                        style: TextStyle(
                          color: c.display.colors.mainPageMenuColor,
                          fontSize: 24,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 16, top: 4, right: 8, bottom: 4),
                        child: Icon(
                          Icons.create,
                          color: c.display.colors.mainPageMenuColor,
                        ),
                      ),
                    ]),
              ),
            ), //end of author textbutton

            //***
            //*** category search textbutton
            //***
            TextButton(
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => CategorySearchScreen())
                ).then((value) {
                  if (value == 'home') {
                    Navigator.of(context).pop('home');
                  }
                });
              },
              child: Padding(
                padding: const EdgeInsets.only(top: 4, right: 8, bottom: 4),
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Text("Category Search",
                        style: TextStyle(
                          color: c.display.colors.mainPageMenuColor,
                          fontSize: 24,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 16, top: 4, right: 8, bottom: 4),
                        child: Icon(
                          Icons.category,
                          color: c.display.colors.mainPageMenuColor,
                        ),
                      ),
                    ]),
              ),
            ), //end of category textbutton


            //***
            //*** narrator search textbutton
            //***
            TextButton(
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => NarratorSearchScreen())
                ).then((value) {
                  if (value == 'home') {
                    Navigator.of(context).pop('home');
                  }
                });
              },
              child: Padding(
                padding: const EdgeInsets.only(top: 4, right: 8, bottom: 4),
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Text("Narrator Search",
                        style: TextStyle(
                          color: c.display.colors.mainPageMenuColor,
                          fontSize: 24,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 16, top: 4, right: 8, bottom: 4),
                        child: Icon(
                          Icons.face,
                          color: c.display.colors.mainPageMenuColor,
                        ),
                      ),
                    ]),
              ),
            ), //end of narrator textbutton

            //***
            //*** what's new search textbutton
            //***
            TextButton(
              onPressed: () {
                var theSearch = ASearchCategory();
                theSearch.setSearchOrder(c.newestFirst);
                theSearch.setCategoryToSearch(c.anyCategory);
                Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => SearchResultScreen(theSearch))
                ).then((value) {
                  if (value == 'home') {
                    Navigator.of(context).pop('home');
                  }
                });
              },
              child: Padding(
                padding: const EdgeInsets.only(top: 4, right: 8, bottom: 4),
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Text("What's New?",
                        style: TextStyle(
                          color: c.display.colors.mainPageMenuColor,
                          fontSize: 24,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 16, top: 4, right: 8, bottom: 4),
                        child: Icon(
                          Icons.question_answer_outlined,
                          color: c.display.colors.mainPageMenuColor,
                        ),
                      ),
                    ]),
              ),
            ), //end of what's new textbutton

            //***********************************
            //* end of search buttons
            //***********************************
          ],
        ),
      ), //end of body container
    );
  }
}
