
import 'package:audiobooks/modules/ASearchCategory.dart';
import 'package:audiobooks/screens/SearchResultScreen.dart';
import 'package:flutter/material.dart';
import 'package:audiobooks/objects/Constants.dart';

class CategorySearchScreen extends StatefulWidget {
  @override
  _CategorySearchScreenState createState() => _CategorySearchScreenState();
}

class _CategorySearchScreenState extends State<CategorySearchScreen> {

  var c = Constants();

  final _catFormKey = GlobalKey<FormState>();
  var _catSortOrderValue = "Newest First";
  var _catCategoryValue = "Any Category";

  //***
  //*** main override
  //***
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        actions: [
          IconButton(
              icon: Icon(Icons.home),
              onPressed: () {
                Navigator.of(context).pop('home');
              }),
        ],
        title: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(c.display.title, style: c.display.textStyleHeaderRegular),
                //c.display.jcusWebsite,
              ],
            ),
          ),
        ),
        toolbarHeight: MediaQuery.of(context).size.height * c.display.appBarHeight,
      ),

      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage(c.backgroundImages.elementAt(4)),
            fit: BoxFit.fitWidth,
            alignment: Alignment.bottomCenter,
          ),
          gradient: LinearGradient(
            colors: [
              c.display.colors.backgroundColor1,
              c.display.colors.backgroundColor2,
              c.display.colors.backgroundColor1,
              c.display.colors.backgroundColor2
            ],
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
          ),
        ),

        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            //**********************************
            //* search form
            //***********************************

            //***************************************************
            //***************************************************
            //* category search
            //***************************************************
            //***************************************************
            Container(
              //padding: const EdgeInsets.only(bottom: 500),
              //color: Colors.greenAccent,
              child: Form(
                key: _catFormKey,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    //***
                    //*** title
                    //***
                    Padding(
                      padding: const EdgeInsets.only(left: 8.0, right: 8.0, bottom: 8.0, top: 12.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Text("Category Search",
                              style: TextStyle(
                                fontSize: 24.0,
                                color: c.display.colors.inputBackgroundColor,
                              )),
                          Padding(
                            padding: const EdgeInsets.only(left: 16, top: 12, right: 8, bottom: 8),
                            child: Icon(
                              Icons.category,
                              color: c.display.colors.mainPageMenuColor,
                            ),
                          ),
                        ],
                      ),
                    ), //end of title padding

                    Row(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: getCatCategoryDropdown(),
                        ),
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 0.0, right: 0.0, top: 0.0, bottom: 32.0),
                      child: Row(
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: getSortOrderDropdown(),
                          ),

                          Spacer(),

                          //*** icon button to perform search
                          Padding(
                            padding: const EdgeInsets.only(left: 8.0, right: 8.0, top: 8.0, bottom: 8.0),
                            child: Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(9.0),
                                border: Border.all(color: c.display.colors.inputBorderColor),
                                color: c.display.colors.inputBackgroundColor,
                              ),
                              child: IconButton(
                                icon: Icon(Icons.search_rounded, size: 25),
                                onPressed: () {
                                  FocusScopeNode currentFocus = FocusScope.of(context);
                                  if (!currentFocus.hasPrimaryFocus) {
                                    currentFocus.unfocus();
                                  }
                                  var theSearch = ASearchCategory();
                                  theSearch.setSearchOrder(_catSortOrderValue);
                                  theSearch.setCategoryToSearch(_catCategoryValue);
                                  Navigator.push(context, MaterialPageRoute(builder: (context) => SearchResultScreen(theSearch)))
                                      .then((value) {
                                    if (value == 'home') {
                                      Navigator.of(context).pop('home');
                                    }
                                  });
                                },
                              ),
                            ),
                          )
                        ],
                      ),

                    ),

                  ],
                ),
              ),
            ),
            //***********************************
            //* end of search form
            //***********************************
          ],
        ),
      ), //end of body container
    );
  } //end of build

//************************************************
//* begining of functions
//************************************************

  //***
  //*** get cat category dropdown
  //***
  Widget getCatCategoryDropdown() {
    return Container(
      width: MediaQuery.of(context).size.width - 16,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(9.0),
        border: Border.all(color: c.display.colors.inputBorderColor),
        color: c.display.colors.inputBackgroundColor,
      ),
      child: Padding(
        padding: const EdgeInsets.only (top: 0.0, bottom: 0.0, left: 8.0, right: 8.0),
        child: DropdownButtonHideUnderline(
          child: DropdownButton<String>(
            dropdownColor: c.display.colors.inputBackgroundColor,
            value: _catCategoryValue,
            icon: Icon(Icons.arrow_drop_down_rounded),
            iconSize: 24,
            elevation: 16,
            style: TextStyle(color: c.display.colors.inputTextColor),
            onChanged: (String newValue) {
              setState(() {
                _catCategoryValue = newValue;
              });
            },
            items:  c.categories.map<DropdownMenuItem<String>>((String value) {
              return DropdownMenuItem<String>(
                value: value,
                child: Text(value),
              );
            }).toList(),
          ),
        ),
      ),
    );
  }

  //***
  //*** get cat sort order dropdown
  //***
  Widget getSortOrderDropdown() {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(9.0),
        border: Border.all(color: c.display.colors.inputBorderColor),
        color: c.display.colors.inputBackgroundColor,
      ),
      //color: Colors.white,
      child: Padding(
        padding: const EdgeInsets.only (top: 0.0, bottom: 0.0, left: 8.0, right: 8.0),
        child: DropdownButtonHideUnderline(
          child: DropdownButton<String>(
            dropdownColor: c.display.colors.inputBackgroundColor,
            value: _catSortOrderValue,
            icon: Icon(Icons.arrow_drop_down_rounded),
            iconSize: 24,
            elevation: 16,
            style: TextStyle(color: c.display.colors.inputTextColor),
            onChanged: (String newValue) {
              setState(() {
                _catSortOrderValue = newValue;
              });
            },
            items: ["Newest First", "Most Popular", "Oldest First"].map<DropdownMenuItem<String>>((String value) {                            //getCategoryList().map<DropdownMenuItem<String>>((String value) {
              return DropdownMenuItem<String>(
                value: value,
                child: Text(value),
              );
            }).toList(),
          ),
        ),
      ),
    );
  }








}
