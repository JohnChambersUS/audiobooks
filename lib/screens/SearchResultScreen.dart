
import 'package:audiobooks/modules/ASearchBase.dart';
import 'package:audiobooks/popups/EmptySearchResultPopup.dart';
import 'package:audiobooks/popups/ServiceIsDownPopup.dart';
import 'package:audiobooks/screens/SearchDetailScreen.dart';
import 'package:flutter/material.dart';
import 'package:audiobooks/objects/Constants.dart';
import 'package:visibility_detector/visibility_detector.dart';
import 'package:cached_network_image/cached_network_image.dart';

class SearchResultScreen extends StatefulWidget {

  ASearchBase _search;

  SearchResultScreen(this._search);

  @override
  _SearchResultScreenState createState() => _SearchResultScreenState(_search);
}

class _SearchResultScreenState extends State<SearchResultScreen> {

  ASearchBase _search;
  String _loadingHide = '';
  bool _loading = true;

  _SearchResultScreenState(this._search) {
    var searchDone = _search.search();
    searchDone.then((value) {
      var lrc = _search.getLastReturnCode();
      if (_search.getLastReturnCode() == 503) {
        _loading = false;
        setState(() {
          _loading;
        });
        Navigator.of(context).push(PageRouteBuilder(
            opaque: false,
            pageBuilder: (BuildContext context, _, __) {
              return ServiceIsDownPopup();
            }
        )).then((value) => Navigator.pop(context));
      } else if (_search.titleList.isEmpty) {
        _loading = false;
        setState(() {
            _loading;
          });
          Navigator.of(context).push(PageRouteBuilder(
              opaque: false,
              pageBuilder: (BuildContext context, _, __) {
                return EmptySearchResultPopup();
              }
          )).then((value) => Navigator.pop(context));
      } else {
            _loading = false;
            setState(() {
              _loading;
              _search.titleList;
            });
      }
    });
  }

  var c = Constants();
  var _searchDone = false;
  var _lastIndexPulled = 0;

  Future<String> _getTotalTime(int index) async {
    var rv = await _search.titleList.elementAt(index).getTotalTime();
    return rv;
  }

  //******** called when reaches the last tile
  void visibleListTile(int index) {
    if (_searchDone) {return;}

    if ((index > _lastIndexPulled) && (index == (_search.titleList.length - 1))) {
      _lastIndexPulled = index;
      _continueSearch();
    }
  }

  void _continueSearch() async {
    var listLength = _search.titleList.length;
    var result = _search.search();
    result.then((value) {
      if (_search.titleList.length > listLength) {
        setState(() {
          _search.titleList;
        });
      } else {
        _searchDone = true;
      }
    });
  }

  AssetImage _getLoadingImage() {
    if (_loading) {
      return AssetImage('assets/loading.gif');
    } else {
      return AssetImage('');
    }
  }

  //***
  //*** get list tile 2
  //***
  Widget _getListTile2(int index) {

    return InkWell(
      onTap: () {
        Navigator.push(context, MaterialPageRoute(builder: (context) => SearchDetailScreen(_search.titleList.elementAt(index))))
          .then((value) {
            if (value == 'home') {
              Navigator.of(context).pop('home');
            }
        });
        },
      child: Row(
        children: [
          //*** image *****************
          Container(
            width: MediaQuery.of(context).size.width * 0.25,
            height: MediaQuery.of(context).size.width * 0.25,
            child: CachedNetworkImage(
              imageBuilder: (context, imageProvider) => Container(
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: imageProvider,
                    fit: BoxFit.fitWidth,
                    alignment: Alignment.centerLeft,
                  ),
                ),
              ),
              imageUrl: _search.titleList.elementAt(index).thumbNailUrl,
              placeholder: (context, url) {return Image.asset('assets/loading.gif');},
            ),
          ),

          //*** middle text *********************
          Expanded(
            child: Container(
              width: MediaQuery.of(context).size.width * 0.50,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(_search.titleList.elementAt(index).title,
                        style: TextStyle(fontWeight: FontWeight.bold, color: c.display.colors.bookRowTextColor)
                    ),
                    Text(_search.titleList.elementAt(index).author,
                        style: TextStyle(fontWeight: FontWeight.normal, color: c.display.colors.bookRowTextColor)
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        _search.titleList.elementAt(index).getRatingStars(),

                        FutureBuilder(
                            future: _getTotalTime(index),
                            initialData: "Loading text..",
                            builder: (BuildContext context, AsyncSnapshot<String> text) {
                              if (text.data != null) {
                                return new Text(text.data, style: TextStyle(
                                    color: c.display.colors
                                        .bookRowTextColor),);
                              } else {
                                return Text("Loading", style: TextStyle(color: c.display.colors.bookRowTextColor));
                              }
                            }
                        ),
                        //Text(_getTotalTime(index), style: TextStyle(color: c.display.colors.bookRowTextColor)),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),

          //*** more icon ***
          Padding(
            padding: const EdgeInsets.only(right: 8.0),
            child: Icon(Icons.arrow_forward_ios, color: c.display.colors.bookRowTextColor),
          ),
        ],
      ),
    );
  }

  //***
  //*** main widget
  //***
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: [
          IconButton(
              icon: Icon(Icons.home),
              onPressed: () {
                Navigator.of(context).pop('home');
              }),
        ],
        title: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(c.display.title, style: c.display.textStyleHeaderRegular),
                //c.display.jcusWebsite,
              ],
            ),
          ),
        ),
        toolbarHeight: MediaQuery.of(context).size.height * c.display.appBarHeight,
      ),

      body: Container(
        height: MediaQuery.of(context).size.height,
        decoration: BoxDecoration(
            image: DecorationImage(
              image: _getLoadingImage(),
              fit: BoxFit.scaleDown,
            ),
            gradient: LinearGradient(
              colors: [
                c.display.colors.backgroundColor1,
                c.display.colors.backgroundColor2,
                c.display.colors.backgroundColor1,
                c.display.colors.backgroundColor2
              ],
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
            )
        ),

        child: Container(
          child: ListView.builder(
            shrinkWrap: true,
            itemCount: _search.titleList.length,    //tsh.length, //todo change to correct
            itemBuilder: (BuildContext context,int index){
              return Container(
                decoration: BoxDecoration(
                  border: Border(bottom: BorderSide(color: c.display.colors.bookRowTextColor)),
                  color: c.display.colors.bookRowColor,
                ),

                child: VisibilityDetector(
                  key: Key("my-visibility-key"),
                  onVisibilityChanged: (visibilityInfo) {
                    visibleListTile(index);
                  },
                  child: _getListTile2(index),
                ), //end of list tile / visibility detector
              );
            } ,
          ),
        ),
      ),
    );
  }

}
