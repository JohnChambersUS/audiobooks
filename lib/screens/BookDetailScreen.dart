
import 'package:audiobooks/objects/ASingleBook.dart';
import 'package:audiobooks/popups/RemoveBookConfirmationPopup.dart';
import 'package:audiobooks/popups/DisplayBookInfoPopup.dart';
import 'package:audiobooks/popups/ResetChaptersConfirmationPopup.dart';
import 'package:audiobooks/popups/SpeedSelectionPopup.dart';
import 'package:audiobooks/popups/SleepTimerPopup.dart';
import 'package:audiobooks/screens/BookReaderScreen.dart';
import 'package:flutter/material.dart';
import 'package:audiobooks/objects/Constants.dart';
import 'package:percent_indicator/percent_indicator.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:audiobooks/popups/BookCompletePopupPlayAll.dart';

class BookDetailScreen extends StatefulWidget {

  ASingleBook _book;

  BookDetailScreen(this._book) {}

  @override
  _BookDetailScreenState createState() => _BookDetailScreenState(_book);
}

class _BookDetailScreenState extends State<BookDetailScreen> {

  ASingleBook _book;
  Constants c = Constants();
  int _sleepTimer = -1;

  _BookDetailScreenState(this._book) {

    var loaded = _book.loadTableOfContents();
    loaded.then((value) {
      setState(() { });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: [
          IconButton(
              icon: Icon(Icons.home),
              onPressed: () {
                Navigator.of(context).pop('home');
              }),
        ],
        title: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(c.display.title, style: c.display.textStyleHeaderRegular),
                //Text(c.display.subTitle, style: c.display.textStyleHeaderSmall),
                //c.display.jcusWebsite,
              ],
            ),
          ),
        ),
        toolbarHeight: MediaQuery.of(context).size.height * c.display.appBarHeight,
      ),

      body: Container(
        decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [
                c.display.colors.backgroundColor1,
                c.display.colors.backgroundColor2,
                c.display.colors.backgroundColor1,
                c.display.colors.backgroundColor2
              ],
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
            )
        ),
        child: CustomScrollView(
        slivers: <Widget> [

          SliverToBoxAdapter(
            child: Container(
              height: MediaQuery.of(context).size.height * 0.30,
              //color: c.display.colors.bookCoverBackgroundColor,
              child: CachedNetworkImage(
                imageBuilder: (context, imageProvider) => Container(
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: imageProvider,
                      fit: BoxFit.fitHeight,
                    ),
                  ),
                ),
                imageUrl: _book.dustJacket.imageUrl,
                placeholder: (context, url) {return Image.asset('assets/loading.gif');},
              ),
            ),
          ),

          SliverToBoxAdapter(
            child: Container(
              color: c.display.colors.menuBarColor,
              height: 70,
              child: ListView(
                scrollDirection: Axis.horizontal,
                shrinkWrap: true,
                children: [

                  Padding(
                    padding: EdgeInsets.only(left: 0.0, right: 0.0, bottom: 8.0, top: 8.0),
                    child: FlatButton(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(Icons.play_arrow_rounded, size: 35.0, color: c.display.colors.menuBarIconColor),
                          Text("Play", style: TextStyle(color: c.display.colors.menuBarIconColor),)
                        ],
                      ),
                      onPressed: () {
                        if (bookComplete(c.theNinesDecision)) {
                        var rv = Navigator.of(context).push(PageRouteBuilder(
                            opaque: false,
                            pageBuilder: (BuildContext context, _, __) {
                              return BookCompletePopupPlayAll();
                            }
                        ));
                        } else {
                          var rc = Navigator.push(context,MaterialPageRoute(builder: (context) => BookReaderScreen(_book, c.theNinesDecision, _sleepTimer)));
                          rc.then((value) {
                            if (value == 'home') {
                              Navigator.of(context).pop('home');
                            }
                            setState(() {});
                          });
                        }
                      },
                    ),
                  ),

                  Padding(
                    padding: EdgeInsets.only(left: 0.0, right: 0.0, bottom: 8.0, top: 8.0),
                    child: FlatButton(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(Icons.replay, size: 35.0, color: c.display.colors.menuBarIconColor),
                          Text("Reset", style: TextStyle(color: c.display.colors.menuBarIconColor),)
                        ],
                      ),
                      onPressed: () {
                        var rv = Navigator.of(context).push(PageRouteBuilder(
                            opaque: false,
                            pageBuilder: (BuildContext context, _, __) {
                              return ResetChaptersConfirmationPopup();
                            }
                        ));
                        rv.then((value) {
                          _book.tableOfContents.resetAllEpisodesToBeginning();
                          setState(() {});
                        });
                      },
                    ),
                  ),

                  Padding(
                    padding: EdgeInsets.only(left: 0.0, right: 0.0, bottom: 8.0, top: 8.0),
                    child: FlatButton(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(Icons.speed, size: 35.0, color: c.display.colors.menuBarIconColor),
                          Text("Speed" , style: TextStyle(color: c.display.colors.menuBarIconColor),)
                        ],
                      ),
                      onPressed: () {
                        var currSpeed = _book.dustJacket.speed;
                        var rv = Navigator.of(context).push(PageRouteBuilder(
                            opaque: false,
                            pageBuilder: (BuildContext context, _, __) {
                              return SpeedSelectionPopup(currSpeed);
                            }
                        ));
                        rv.then((value) {
                          _book.dustJacket.speed = value;
                          _book.writeToDatabase();
                        });
                      },
                    ),
                  ),

                  //***** sleep timer *****
                  Padding(
                    padding: EdgeInsets.only(left: 0.0, right: 0.0, bottom: 8.0, top: 8.0),
                    child: FlatButton(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(Icons.access_time, size: 35.0, color: c.display.colors.menuBarIconColor),
                          Text("Sleep" , style: TextStyle(color: c.display.colors.menuBarIconColor),)
                        ],
                      ),
                      onPressed: () {
                        var currSpeed = _book.dustJacket.speed;
                        var rv = Navigator.of(context).push(PageRouteBuilder(
                            opaque: false,
                            pageBuilder: (BuildContext context, _, __) {
                              return SleepTimerPopup();
                            }
                        ));
                        rv.then((value) {
                          _sleepTimer = value;
                        });
                      },
                    ),
                  ),

                  Padding(
                    padding: EdgeInsets.only(left: 0.0, right: 0.0, bottom: 8.0, top: 8.0),
                    child: FlatButton(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(Icons.info_outline, size: 35.0, color: c.display.colors.menuBarIconColor),
                          Text("Info", style: TextStyle(color: c.display.colors.menuBarIconColor),)
                        ],
                      ),
                      onPressed: () {
                        var rv = Navigator.of(context).push(PageRouteBuilder(
                            opaque: false,
                            pageBuilder: (BuildContext context, _, __) {
                              return DisplayBookInfoPopup(_book.dustJacket.title, _book.dustJacket.description);
                            }
                        ));
                      },
                    ),
                  ),

                  Padding(
                    padding: EdgeInsets.only(left: 0.0, right: 0.0, bottom: 8.0, top: 8.0),
                    child: FlatButton(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(Icons.remove, size: 35.0, color: c.display.colors.menuBarIconColor),
                          Text("Remove", style: TextStyle(color: c.display.colors.menuBarIconColor),)
                        ],
                      ),
                      onPressed: () {
                        var rv = Navigator.of(context).push(PageRouteBuilder(
                            opaque: false,
                            pageBuilder: (BuildContext context, _, __) {
                              return RemoveBookConfirmationPopup();
                            }
                        ));
                        rv.then((value) {
                          if (value == true) {
                            var rc = _book.deleteYourself();
                            rv.then((value) => Navigator.of(context).pop('deleted'));
                          }
                        });

                      },
                    ),
                  ),
                ],
              ),
            ),
          ),

          SliverList(
            delegate: SliverChildBuilderDelegate((BuildContext context, int index) {
              return Container(
                decoration: BoxDecoration(
                  border: Border(
                      bottom: BorderSide(color: c.display.colors.bookRowTextColor)
                  ),
                  color: c.display.colors.bookRowColor
                ),
                child:  ListTile(

                      leading: InkWell(
                        onTap: () {
                          _book.tableOfContents.chapters.elementAt(index).playPoint = 0;
                          setState(() {});
                        },
                        child: CircularPercentIndicator(
                          radius: 35.0,
                          lineWidth: 5.0,
                          percent: setPlayedPercentage(index).toDouble(),
                          progressColor: c.display.colors.circleColor,
                        ),
                      ),

                      title: Column(
                        children: [
                          Text(
                            _book.tableOfContents.chapters.elementAt(index).title,
                              style: TextStyle(color: c.display.colors.bookRowTextColor)
                           ),
                          Text(
                              _book.tableOfContents.chapters.elementAt(index).savedLength,
                              style: TextStyle(color: c.display.colors.bookRowTextColor)
                          ),
                        ],
                      ),

                      trailing: InkWell(
                          onTap: () {
                            if (!bookComplete(index)) {
                              var rc = Navigator.push(context,
                                  MaterialPageRoute(builder: (context) =>
                                      BookReaderScreen(_book, index, _sleepTimer)));
                              rc.then((value) {
                                if (value == 'home') {
                                  Navigator.of(context).pop('home');
                                }
                                setState(() {});
                              });
                            }
                          },
                          child: Icon(Icons.play_arrow_rounded, size: 35, color: c.display.colors.bookRowTextColor,)
                      ),
                  ),
                );

            },
              childCount: _book.tableOfContents.chapters.length,    // 100, //entryHelper.episodes.length,
            ), //end of sliver child buider delegate
          ), //end of sliver list
        ]),
      ),
    );

  }

  bool bookComplete(int index) {
    return _book.isComplete(index);
  }

  double setPlayedPercentage(int i) {
    var playPoint = _book.tableOfContents.chapters.elementAt(i).playPoint;
    if (playPoint == 0) {return 0;}
    var duration = _book.tableOfContents.chapters.elementAt(i).duration;
    if (duration == 1) {
      _book.tableOfContents.updatePlayPoint(0);
      return 0.0;
    }
    if (playPoint > duration) {return 1.0;}
    var pct = playPoint / duration;
    return pct;
  }


}
