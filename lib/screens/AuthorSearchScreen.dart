
import 'package:audiobooks/modules/ASearchAuthor.dart';
import 'package:audiobooks/screens/SearchResultScreen.dart';
import 'package:flutter/material.dart';
import 'package:audiobooks/objects/Constants.dart';

class AuthorSearchScreen extends StatefulWidget {
  @override
  _AuthorSearchScreenState createState() => _AuthorSearchScreenState();
}

class _AuthorSearchScreenState extends State<AuthorSearchScreen> {

  var c = Constants();

  final _authorFormKey = GlobalKey<FormState>();
  var _authorDropDownValue = "Contains";
  var _authorCategoryValue = "Any Category";
  var _authorTextController = TextEditingController();

  //***
  //*** main override
  //***
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        actions: [
          IconButton(
              icon: Icon(Icons.home),
              onPressed: () {
                Navigator.of(context).pop('home');
              }),
        ],
        title: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(c.display.title, style: c.display.textStyleHeaderRegular),
                //Text(c.display.subTitle, style: c.display.textStyleHeaderSmall),
                //c.display.jcusWebsite,
              ],
            ),
          ),
        ),
        toolbarHeight: MediaQuery.of(context).size.height * c.display.appBarHeight,
      ),

      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage(c.backgroundImages.elementAt(3)),
            fit: BoxFit.fitWidth,
            alignment: Alignment.bottomCenter,
          ),
          gradient: LinearGradient(
            colors: [
              c.display.colors.backgroundColor1,
              c.display.colors.backgroundColor2,
              c.display.colors.backgroundColor1,
              c.display.colors.backgroundColor2
            ],
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
          ),
        ),

        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            //**********************************
            //* search form
            //***********************************


            //**********************************************
            //**********************************************
            //* Author search fields
            //**********************************************
            //**********************************************

            Padding(
              padding: const EdgeInsets.only(bottom: 0.0, top: 0.0, left: 0.0, right: 0.0),
              child: Container(
                //padding: const EdgeInsets.only(bottom: 500),
                  //color: Colors.white24,
                  child: Form(
                      key: _authorFormKey,
                      child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            //*** title ***
                            Padding(
                              padding: const EdgeInsets.only(left: 8.0, right: 8.0, bottom: 8.0, top: 12.0),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  Text("Author Search",
                                      style: TextStyle(
                                        fontSize: 25.0,
                                        color: c.display.colors.inputBackgroundColor,
                                      )),
                                  Padding(
                                    padding: const EdgeInsets.only(left: 16, top: 12, right: 8, bottom: 8),
                                    child: Icon(
                                      Icons.create,
                                      color: c.display.colors.mainPageMenuColor,
                                    ),
                                  ),
                                ],
                              ),
                            ),

                            //*** input box ***
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Container(
                                child: TextFormField(
                                  controller: _authorTextController,
                                  decoration: InputDecoration(
                                    hintText: "Enter an Author",
                                    border: OutlineInputBorder(borderRadius: BorderRadius.circular(9.0), gapPadding: 8.0,  ) ,
                                    contentPadding: EdgeInsets.only(left: 12.0, right: 12.0, bottom: 12.0, top: 12.0),
                                    fillColor: c.display.colors.inputBackgroundColor,
                                    filled: true,
                                  ),
                                  validator: (value) {
                                    if (value.isEmpty) {
                                      return 'Please enter some text';
                                    }
                                    return null;
                                  },
                                ),
                              ),
                            ),

                            //*** drop down for position in string
                            Padding(
                              padding: const EdgeInsets.only(left: 0.0, right: 0.0, top: 0.0, bottom: 32.0),
                              child: Row(
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: getAuthorCategoryDropdown(),
                                  ),

                                  Spacer(),

                                  //*** icon button to perform search
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Container(
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(9.0),
                                        border: Border.all(color: c.display.colors.inputBorderColor),
                                        color: c.display.colors.inputBackgroundColor,
                                      ),
                                      child: IconButton(
                                        icon: Icon(Icons.search_rounded, size: 25),
                                        onPressed: () {
                                          if ( _authorTextController.value.text.length > 0) {
                                            FocusScopeNode currentFocus = FocusScope.of(context);
                                            if (!currentFocus.hasPrimaryFocus) {
                                              currentFocus.unfocus();
                                            }
                                            var theSearch = ASearchAuthor();
                                            theSearch.setAuthorToSearch(_authorTextController.value.text);
                                            theSearch.setCategoryToSearch(_authorCategoryValue);
                                            Navigator.push(context, MaterialPageRoute(builder: (context) => SearchResultScreen(theSearch)))
                                                .then((value) {
                                              if (value == 'home') {
                                                Navigator.of(context).pop('home');
                                              }
                                            });
                                          }
                                        },
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ]
                      )
                  )
              ),
            ),

          ],

        ),

      ), //end of body container

    );
  } //end of build

//************************************************
//* begining of functions
//************************************************

  //***
  //*** get author category dropdown
  //***
  Widget getAuthorCategoryDropdown() {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(9.0),
        border: Border.all(color: c.display.colors.inputBorderColor),
        color: c.display.colors.inputBackgroundColor,
      ),
      child: Padding(
        padding: const EdgeInsets.only (top: 0.0, bottom: 0.0, left: 8.0, right: 8.0),
        child: DropdownButtonHideUnderline(
          child: DropdownButton<String>(
            dropdownColor: c.display.colors.inputBackgroundColor,
            value: _authorCategoryValue,
            icon: Icon(Icons.arrow_drop_down_rounded),
            iconSize: 24,
            elevation: 16,
            style: TextStyle(color: c.display.colors.inputTextColor),
            onChanged: (String newValue) {
              setState(() {
                _authorCategoryValue = newValue;
              });
            },
            items: c.categories.map<DropdownMenuItem<String>>((String value) {
              return DropdownMenuItem<String>(
                value: value,
                child: Text(value),
              );
            }).toList(),
          ),
        ),
      ),
    );
  }

}
