import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:audiobooks/popups/EmptyLibraryPopup.dart';
import 'package:audiobooks/screens/BookDetailScreen.dart';
import 'package:audiobooks/objects/Constants.dart';
import 'package:audiobooks/objects/Library.dart';

class LibraryScreen extends StatefulWidget {
  @override
  _LibraryScreenState createState() => _LibraryScreenState();
}

class _LibraryScreenState extends State<LibraryScreen> {

  Library _library;
  var c = Constants();

  _LibraryScreenState() {
    initLibrary();
  }

  void initLibrary() {
    _library = Library();
    var done = _library.loadBooks();
    done.then((value) {
      if (_library.books.isEmpty) {
        var rv = Navigator.of(context).push(PageRouteBuilder(
            opaque: false,
            pageBuilder: (BuildContext context, _, __) {
              return EmptyLibraryPopup();
            }
        ));
        rv.then((value) => Navigator.of(context).pop());
      }
      setState(() {
        _library.books;
      });
    });
  }

  Widget _getListTileItem(int index) {
    return Container(
      decoration: BoxDecoration(
        border: Border(bottom: BorderSide(color: c.display.colors.bookRowTextColor)),
        color: c.display.colors.bookRowColor,
      ),
      child: InkWell(

        onTap: () {
          var deleted = Navigator.push(context, MaterialPageRoute(builder: (context) => BookDetailScreen(_library.books.elementAt(index))));
          deleted.then((value) {
            if (value != null) {
              if (value == 'deleted') {
                initLibrary();
              }
              if (value == 'home') {
                Navigator.of(context).pop('home');
              }
            }
          });
        },
        child: Row(
          children: [
            //*** image *****************
            Container(
              width: MediaQuery.of(context).size.width * 0.25,
              height: MediaQuery.of(context).size.width * 0.25,
              child: CachedNetworkImage(
                imageBuilder: (context, imageProvider) => Container(
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: imageProvider,
                      fit: BoxFit.fitWidth,
                      alignment: Alignment.centerLeft,
                    ),
                  ),
                ),
                imageUrl: _library.books.elementAt(index).dustJacket.imageUrl,
                placeholder: (context, url) {return Image.asset('assets/loading.gif');},
              ),
            ),
            //*** main text ***********************
            Expanded(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(_library.books.elementAt(index).dustJacket.title,          //tsh.titleList.elementAt(index).title,
                        style: TextStyle(fontWeight: FontWeight.bold, color: c.display.colors.bookRowTextColor)
                    ),
                    Text(_library.books.elementAt(index).dustJacket.author,     //tsh.titleList.elementAt(index).author,
                        style: TextStyle(fontWeight: FontWeight.normal, color: c.display.colors.bookRowTextColor)
                    ),
                  ],
                ),
              ),
            ),
            //*** icon more button ****************************
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Icon(Icons.arrow_forward_ios, color: c.display.colors.bookRowTextColor),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: [
          IconButton(
              icon: Icon(Icons.home),
              onPressed: () {
                Navigator.of(context).pop('home');
              }),
        ],
        title: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(c.display.title, style: c.display.textStyleHeaderRegular),
                //Text(c.display.subTitle, style: c.display.textStyleHeaderSmall),
                //c.display.jcusWebsite,
              ],
            ),
          ),
        ),
        toolbarHeight: MediaQuery.of(context).size.height * c.display.appBarHeight,
      ),

      body: Container(
        decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [
                c.display.colors.backgroundColor1,
                c.display.colors.backgroundColor2,
                c.display.colors.backgroundColor1,
                c.display.colors.backgroundColor2
              ],
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
            )
        ),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Expanded(
                child: Container(
                  child: ListView.builder(
                    shrinkWrap: true,
                    itemCount: _library.books.length,
                    itemBuilder: (BuildContext context,int index){
                      return _getListTileItem(index);
                    } ,
                  ),
                ),
              ),
            ]
          ),
        ),
      ),
    );
  }
}

