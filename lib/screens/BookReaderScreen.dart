import 'dart:async';
import 'package:audiobooks/objects/ASingleBook.dart';
import 'package:audiobooks/popups/BookCompletePopup.dart';
import 'package:flutter/material.dart';
import 'package:audiobooks/objects/Constants.dart';
import 'dart:ui';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:assets_audio_player/assets_audio_player.dart';
import 'dart:io' show Platform;
import 'package:flutter_background/flutter_background.dart';

class BookReaderScreen extends StatefulWidget {

  ASingleBook _book;
  int _startingIndex;
  int _sleepTimer;

  BookReaderScreen(this._book, this._startingIndex, this._sleepTimer) {}

  @override
  _BookReaderScreenState createState() => _BookReaderScreenState(_book, _startingIndex, _sleepTimer);
}

class _BookReaderScreenState extends State<BookReaderScreen> {

  var c = Constants();

  bool _done = false;

  ASingleBook _book;
  int _startingIndex;
  int _sleepTimer;

  AssetsAudioPlayer _assetsAudioPlayer = AssetsAudioPlayer();
  final List<StreamSubscription> _subscriptions = [];

  String _currentPosition = "0";
  int _currentPositionInt = 0;

  String _duration = "0";
  int _durationInt = 10800;

  Duration _playPoint = Duration();
  Duration _endPoint = Duration();

  var _resumeOffstage = true;
  var _pauseOffstage = false;

  var _speed = 1.0;
  var _speedDisplay = "1.00x";

  String exitCode = "";

  int _benchSeconds = 0;

  _BookReaderScreenState(this._book, this._startingIndex, this._sleepTimer) {

    if (Platform.isAndroid) {
      initBackgroundPlay();
    }

    _book.tableOfContents.setNextEpisode(_startingIndex);
    initSubscriptions();
    play();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        _done = true;
        stopAndExit();
      },
      child: Scaffold(
        appBar: AppBar(
          actions: [
            IconButton(
                icon: Icon(Icons.home),
                onPressed: () {
                  exitCode = 'home';
                  stopAndExit();
                }),
          ],
          title: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(trimTitle(c.display.title), style: c.display.textStyleHeaderRegular),
                  //c.display.jcusWebsite,
                ],
              ),
            ),
          ),
          toolbarHeight: MediaQuery.of(context).size.height * c.display.appBarHeight,
        ),

        body: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [
                c.display.colors.backgroundColor1,
                c.display.colors.backgroundColor2,
                c.display.colors.backgroundColor1,
                c.display.colors.backgroundColor2
              ],
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
            )
          ),
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[

                Container(
                  //color: Colors.black87,
                  height: MediaQuery.of(context).size.height * 0.30,
                  child: Center(
                    child: CachedNetworkImage(
                      imageBuilder: (context, imageProvider) => Container(
                        decoration: BoxDecoration(
                          image: DecorationImage(
                              image: imageProvider,
                              fit: BoxFit.fitHeight,
                          ),
                        ),
                      ),
                      imageUrl: _book.dustJacket.imageUrl,
                      placeholder: (context, url) {return Image.asset('assets/loading.gif');},
                    ),
                  ),
                ),

                Spacer(),

                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Center(
                    child: Text(_book.tableOfContents.getCurrentTitle(),
                        style: TextStyle(fontWeight: FontWeight.bold, fontSize: 25, color: c.display.colors.playerTitleColor)),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(0.0),
                      child: FlatButton(
                        //color: Colors.greenAccent,
                        child: Icon(Icons.replay_30_rounded,
                          size: MediaQuery.of(context).size.width * 0.16,
                          color: c.display.colors.playerTitleColor
                        ),
                        onPressed: () {
                          Duration d = Duration(seconds: _playPoint.inSeconds - 30);
                          _assetsAudioPlayer.seek(d); //tpdpo audioPlayer.seek(d);
                        },
                      ),
                    ),
                    Offstage(
                      offstage: _pauseOffstage,
                      child: FlatButton(
                        child: Icon(Icons.pause_circle_filled_rounded,
                            size: MediaQuery.of(context).size.width * 0.25,
                            color: c.display.colors.playerTitleColor
                        ),
                        shape: CircleBorder(),
                        onPressed: () {
                          pause();
                        },
                      ),
                    ),

                    Offstage(
                      offstage: _resumeOffstage,
                      child: FlatButton(
                        child: Icon(Icons.play_circle_filled_rounded,
                            size: MediaQuery.of(context).size.width * 0.25,
                            color: c.display.colors.playerTitleColor
                        ),
                        shape: CircleBorder(),
                        onPressed: () {
                          resume();
                        },
                      ),
                    ), //end of flatbutton
                    Padding(
                      padding: const EdgeInsets.all(0.0),
                      child: FlatButton(
                        child: Icon(Icons.forward_30_rounded,
                          size: MediaQuery.of(context).size.width * 0.16,
                          color: c.display.colors.playerTitleColor
                        ),
                        onPressed: () {
                          Duration d = Duration(seconds: _playPoint.inSeconds + 30);
                          _assetsAudioPlayer.seek(d); //todo audioPlayer.seek(d);
                        },
                      ),
                    ),
                  ],
                ),
                //*** slider **********
                Padding(
                  padding: const EdgeInsets.only(bottom: 0.0),
                  child: Container(
                    width:  MediaQuery.of(context).size.width,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [

                        Slider(
                          activeColor: c.display.colors.playerTitleColor,
                          inactiveColor: c.display.colors.sliderInactiveColor,
                          value: _currentPositionInt.toDouble(),
                          min: 0,
                          max: _durationInt.toDouble(),
                          divisions: _durationInt,
                          label: _currentPositionInt.toString(),
                          onChanged: (double value) {
                            setState(() {
                              //pause();
                              _currentPositionInt = value.toInt();
                              var _decrement = _currentPositionInt % 60;
                              var durr = Duration(seconds: value.toInt());
                              _assetsAudioPlayer.seek(durr); //todo audioPlayer.seek(durr);
                              resume();
                            });
                          },
                          onChangeEnd: (double value) {
                            var x = 1;
                          },
                        ), // end of slider

                        Padding(
                          padding: EdgeInsets.only(left: 25.0, right: 25.0, bottom: 0.0, top: 0.0),
                          child: Row(
                            children: [
                              Text(_currentPosition.toString(), style: TextStyle(color: c.display.colors.playerTitleColor)),
                              //space
                              Spacer(),
                              //total time
                              Text(_duration.toString(), style: TextStyle(color: c.display.colors.playerTitleColor)),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Spacer(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  //**********************************************
  //* init backgroud play
  //**********************************************
  initBackgroundPlay() async {

    bool hasPermissions = await FlutterBackground.hasPermissions;

    if (hasPermissions == false) {
      final androidConfig = FlutterBackgroundAndroidConfig(
        notificationTitle: "flutter_background example app",
        notificationText: "Background notification for keeping the example app running in the background",
        notificationImportance: AndroidNotificationImportance.Default,
        notificationIcon: AndroidResource(name: 'background_icon',
            defType: 'drawable'), // Default is ic_launcher from folder mipmap
      );
      bool success = await FlutterBackground.initialize(
          androidConfig: androidConfig);
    }

  }

  //*******************************************
  //* Set listeners for assets audio player
  //*******************************************
  initSubscriptions() {

    _subscriptions.add(_assetsAudioPlayer.playlistFinished.listen((data) {
      if ((data == true) &&(_currentPositionInt > 0)) {
        writeEpisodes();
        if (_done == true) return;
        var more = _book.tableOfContents.incrementEpisode();
        if (!more) {
          //stopAndExit();
          Navigator.of(context).push(PageRouteBuilder(
              opaque: false,
              pageBuilder: (BuildContext context, _, __) {
                return BookCompletePopup();
              }
          )).then((value) {
            stopAndExit();
          });
        } else {
          play();
        }
      }
    }));

    //todo **** Removed because doesn't work on apple - file bug report with asset_audio develper
    //_subscriptions.add(_assetsAudioPlayer.playlistAudioFinished.listen((data) {
    //
    //}));

    //current song info including duration
    _subscriptions.add(_assetsAudioPlayer.currentPosition.listen((data) {
      try {
        setDurationOnSlider(_assetsAudioPlayer.current.value.audio.duration);
        setCurrentPositionOnSlider(data);
      }
      catch (e) {
        var x = e;
      }
    }));

    //current song info including duration
    _subscriptions.add(_assetsAudioPlayer.isPlaying.listen((data) {
      if (data == false) {
        setState(() {
          _pauseOffstage = true;
          _resumeOffstage = false;
        });
      } else {
        setState(() {
          _pauseOffstage = false;
          _resumeOffstage = true;
        });
      }
    }));

  }

  //****************************************************
  //* setting duration on slider assets_audio version
  //****************************************************
  setDurationOnSlider(Duration duration) {
    setState(() {
      _durationInt = duration.inSeconds;
      String twoDigits(int n) => n.toString().padLeft(2, "0");
      var hours = "";
      if (duration.inHours > 0) {
        hours = duration.inHours.toString() + ":";
      }

      var minutes = "";
      if (duration.inHours > 0) {
        minutes = twoDigits(duration.inMinutes.remainder(60)) + ":";
      } else {
        minutes = duration.inMinutes.remainder(60).toString() + ":";
      }

      var seconds = "";
      if (duration.inMinutes.remainder(60) > 0) {
        seconds = twoDigits(duration.inSeconds.remainder(60));
      } else {
        seconds = duration.inSeconds.remainder(60).toString();
      }
      _duration = hours + minutes + seconds;
    });
  }
  
  //****************************************************
  //* set current position on slider assets audio
  //****************************************************
  setCurrentPositionOnSlider(Duration pp) {
    setState(() {
      _currentPositionInt = pp.inSeconds;
      _playPoint = pp;
      String twoDigits(int n) => n.toString().padLeft(2, "0");
      String twoDigitSeconds = twoDigits(pp.inSeconds.remainder(60));
      if (pp.inHours > 0) {
        _currentPosition = "${pp.inHours}:${twoDigits(pp.inMinutes.remainder(60))}:$twoDigitSeconds";
      } else {
        _currentPosition = "${pp.inMinutes.remainder(60)}:$twoDigitSeconds";
      }
    });
    var seconds = pp.inSeconds;
    if (seconds != _benchSeconds) {
      _benchSeconds = seconds;
      var decrementTime = seconds % 60;
      if (decrementTime == 0) {
        _sleepTimer--;
        if (_sleepTimer == 0) {
          stopAndExit();
        }
      }
    }
  }

  //****************************************
  //* play
  //****************************************
  play() async {
    var url = _book.tableOfContents.getCurrentUrl();
    if (url == "") {
      stopAndExit();
    }
    _speed = _book.dustJacket.speed;
    _assetsAudioPlayer.stop();

    var audios = <Audio>[
      Audio.network(url,
        metas: Metas(
          title: _book.tableOfContents.getCurrentTitle(),
          artist: _book.dustJacket.title,
          image: MetasImage.network(_book.dustJacket.imageUrl),
        ),
      ),
    ];

    //initSubscriptions();
    var pp = _book.tableOfContents.getPlayPoint();
    Duration newPlayPoint = Duration(seconds: _book.tableOfContents.getPlayPoint());
    if (newPlayPoint.inSeconds > _durationInt) {
      _durationInt += newPlayPoint.inSeconds;
    }

    await _assetsAudioPlayer.open(
      Playlist(audios: audios, startIndex: 0),
      playSpeed: _book.dustJacket.speed,
      //seek: newPlayPoint,  //*** doesn't work on apple
      showNotification: true,
      notificationSettings: NotificationSettings(
        playPauseEnabled: true,
        stopEnabled: false,
        customPrevAction: (player) {
          rewind30Seconds();
        },
        customNextAction: (player) {
          fastForward30Seconds();
        },
      ),
      autoStart: true,
    );
    //todo android fix for play buttons being out of sync
    pause();
    _assetsAudioPlayer.seek(newPlayPoint); //put in for apple
    await Future.delayed(const Duration(seconds: 1));
    resume();
  }

  //******************************************
  //* rewinds player 30 seconds
  //******************************************
  rewind30Seconds() {
    var ppis = _playPoint.inSeconds - 30;
    if (ppis < 0) {ppis = 0;}
    Duration d = Duration(seconds: ppis);
    _assetsAudioPlayer.seek(d);
  }

  //******************************************
  //* fast forward player 30 seconds
  //******************************************
  fastForward30Seconds() {
    var ppis = _playPoint.inSeconds + 30;
    if (ppis > _durationInt) {ppis = _durationInt;};
    Duration d = Duration(seconds: ppis);
    _assetsAudioPlayer.seek(d);
  }

  //*****************************************
  //* pause playback
  //*****************************************
  pause() async {
    _assetsAudioPlayer.pause();
    writeEpisodes();
  }

  //******************************************
  //* resume playback
  //******************************************
  resume() async {
    _assetsAudioPlayer.play();
  }

  //*****************************************
  //* update current position
  //*****************************************
  String setDurationToReadableTime(Duration duration) {
    var rv = "";
    var h = 0;
    var m = 1;
    var s = 2;
    var text = duration.toString();
    var parts = text.split(".");
    var times = parts.first.split(":");
    if (int.parse(times.elementAt(h)) > 0) {
      times.first = int.parse(times.elementAt(h)).toString(); //removes leading zero
      rv = times.join(":");
    } else {
      times[m] = int.parse(times.elementAt(m)).toString(); //removes leading zero
      rv = times[m] + ":" + times[s];
    }
    return rv;
  }

  int convertDurationToSeconds(Duration duration) {
    var rv = 0;
    var h = 0;
    var m = 1;
    var s = 2;
    var text = duration.toString();
    var parts = text.split(".");
    var times = parts.first.split(":");
    rv = int.parse(times.elementAt(h)) * 60 * 60;
    rv += int.parse(times.elementAt(m)) * 60;
    rv += int.parse(times.elementAt(s));
    return rv;
  }

  //**********************************
  //* write episodes
  //**********************************
  void writeEpisodes() {
    //var ppis = _playPoint.inSeconds;
    //var dur = _durationInt;
    _book.tableOfContents.updatePlayPoint(_playPoint.inSeconds);
    _book.tableOfContents.updateDuration(_durationInt);
    _book.dustJacket.speed = _speed;
    _book.writeToDatabase();
  }

  //*****************************************
  //* set speed
  //*****************************************
  String decrementSpeed() {
    _speed -= 0.1;
    if (_speed < 0.5) {_speed = 0.5;}
    _speedDisplay = _speed.toStringAsFixed(2);
    _speed = double.parse(_speedDisplay);
    setState(() {
       _speed;
    });
  }

  String incrementSpeed() {
    _speed += 0.1;
    if (_speed >= 2.1) {_speed = 2.0;}
    _speedDisplay = _speed.toStringAsFixed(2);
    _speed = double.parse(_speedDisplay);
    //todo audioPlayer.setPlaybackRate(playbackRate: _speed);
    _pauseOffstage = false;
    _resumeOffstage = true;
    setState(() {
      _speed;
    });
  }

  //******************************************
  //* trim title
  //******************************************
  String trimTitle(String title) {
    var newTitle = "";
    var parts = title.split(" ");
    parts.forEach((element) {
      if (newTitle.length < 76) newTitle += element + " ";
    });
    return newTitle.trim();
  }

  //*****************************************
  //* stop & release player
  //*****************************************
  stopAndExit()  {
    _assetsAudioPlayer.pause();
    _assetsAudioPlayer.stop();
    _assetsAudioPlayer.dispose();
    writeEpisodes();
    Navigator.of(context).pop(exitCode);
  }

}
