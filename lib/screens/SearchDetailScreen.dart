import 'package:audiobooks/modules/EntryDetailHelper.dart';
import 'package:audiobooks/modules/LocalFileHelper.dart';
import 'package:audiobooks/modules/HistoryFile.dart';
import 'package:audiobooks/objects/TitleSearchEntry.dart';
import 'package:audiobooks/objects/Constants.dart';
import 'package:audiobooks/popups/DisplayBookInfoPopup.dart';
import 'package:audiobooks/popups/BookInLibraryPopup.dart';
import 'package:audiobooks/screens/PlayAudioOnDemandScreen2.dart';
import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:audiobooks/popups/AddConfirmationPopup.dart';
import 'package:audiobooks/popups/PreviewErrorPopup.dart';

class SearchDetailScreen extends StatefulWidget {

  TitleSearchEntry _passedEntry;

  SearchDetailScreen(TitleSearchEntry entry) {
    _passedEntry = entry;
  }

  @override
  _SearchDetailScreenState createState() => _SearchDetailScreenState(_passedEntry);
}

class _SearchDetailScreenState extends State<SearchDetailScreen> {

  TitleSearchEntry _passedEntry;
  Constants c = Constants();
  EntryDetailHelper entryHelper;
  //String imageUrl = "";

  _SearchDetailScreenState(TitleSearchEntry entry) {
    _passedEntry = entry;
    entryHelper = EntryDetailHelper(_passedEntry);
    _getEpisodes();
  }

  void _getEpisodes() {
    var rs = entryHelper.getEpisodes();
    rs.then((value) {
      var x = 1;
      setState(() {});
    });
  }

  void _addBookToDatabase() async {
    var lfh = LocalFileHelper();
    lfh.isBookInLibrary(_passedEntry.id).then((bookIsInLibrary) async {
      if (bookIsInLibrary == true) {
        var rv = Navigator.of(context).push(PageRouteBuilder(
            opaque: false,
            pageBuilder: (BuildContext context, _, __) {
              return BookInLibraryPopup();
            }
        ));
      } else {
          lfh.writeDustJacketFile(_passedEntry);
          lfh.writeEpisodesFile(entryHelper.episodes);
          HistoryFile historyFile = HistoryFile();
          await historyFile.isBookInHistory(_passedEntry.id).then((value) {
            if (value == false) {
              historyFile.addToHistory(_passedEntry);
            }
          });
          Navigator.of(context).push(PageRouteBuilder(
              opaque: false,
              pageBuilder: (BuildContext context, _, __) {
                return AddConfirmationPopup();
              }
          )).then((value) => Navigator.of(context).pop());
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: [
          IconButton(
              icon: Icon(Icons.home),
              onPressed: () {
                Navigator.of(context).pop('home');
              }),
        ],
        title: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(c.display.title, style: c.display.textStyleHeaderRegular),
                //Text(c.display.subTitle, style: c.display.textStyleHeaderSmall),
                //c.display.jcusWebsite,
              ],
            ),
          ),
        ),
        toolbarHeight: MediaQuery.of(context).size.height * c.display.appBarHeight,
      ),
      body: Container(
        decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [
                c.display.colors.backgroundColor1,
                c.display.colors.backgroundColor2,
                c.display.colors.backgroundColor1,
                c.display.colors.backgroundColor2
              ],
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
            )
        ),
        child: CustomScrollView(

          slivers: <Widget> [
            //***
            //*** image
            //***
            SliverToBoxAdapter(
              child: Container(
                height: MediaQuery.of(context).size.height * 0.30,
                //color: c.display.colors.bookCoverBackgroundColor,
                child: CachedNetworkImage(
                  imageBuilder: (context, imageProvider) => Container(
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: imageProvider,
                        fit: BoxFit.fitHeight,
                      ),
                    ),
                  ),
                  imageUrl: _passedEntry.thumbNailUrl,
                  placeholder: (context, url) {return Image.asset('assets/loading.gif');},
                ),
              ),
            ),
            //***
            //*** menu bar
            //***
            SliverToBoxAdapter(
              child: Container(
                color: c.display.colors.menuBarColor,
                child: Row(
                  children: [
                    Spacer(),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: FlatButton(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(Icons.add, size: 35.0, color: c.display.colors.menuBarIconColor,),
                            Text("Add",
                              style: TextStyle(
                                color: c.display.colors.menuBarIconColor
                              ),
                            )
                          ],
                        ),
                        onPressed: () {
                          _addBookToDatabase();
                        },
                      ),
                    ),
                    Spacer(),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: FlatButton(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(Icons.info_outline, size: 35.0, color: c.display.colors.menuBarIconColor),
                            Text("Info",
                              style: TextStyle(
                                color: c.display.colors.menuBarIconColor
                              ),
                            )
                          ],
                        ),
                        onPressed: () {
                          var rv = Navigator.of(context).push(PageRouteBuilder(
                              opaque: false,
                              pageBuilder: (BuildContext context, _, __) {
                                return DisplayBookInfoPopup(_passedEntry.title, _passedEntry.description);
                              }
                          ));
                        },
                      ),
                    ),
                    Spacer(),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: FlatButton(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(Icons.play_arrow_rounded, size: 35.0, color: c.display.colors.menuBarIconColor),
                            Text("Preview",
                              style: TextStyle(
                                  color: c.display.colors.menuBarIconColor
                              ),
                            )
                          ],
                        ),
                        onPressed: () {
                          try {
                            if (entryHelper.episodes.isEmpty) throw new Exception();
                            Navigator.push(context,
                                MaterialPageRoute(builder: (context) =>
                                    PlayAudioOnDemandScreen2(
                                        entryHelper.episodes.elementAt(0),
                                        _passedEntry.thumbNailUrl,
                                        _passedEntry.title)))
                                .then((value) {
                              if (value == 'home') {
                                Navigator.of(context).pop('home');
                              }
                            });
                          } catch (e) {
                            var rv = Navigator.of(context).push(PageRouteBuilder(
                                opaque: false,
                                pageBuilder: (BuildContext context, _, __) {
                                  return PreviewErrorPopup();
                                }
                            ));
                          }
                        },
                      ),
                    ),
                    Spacer(),
                  ],
                ),
              ),
            ),

            SliverList(
              delegate: SliverChildBuilderDelegate((BuildContext context, int index) {
                return Container(
                  decoration: BoxDecoration( //                    <-- BoxDecoration
                    border: Border(bottom: BorderSide(color: c.display.colors.bookRowTextColor)),
                    color: c.display.colors.bookRowColor,
                  ),
                  child: InkWell(
                      onTap: () {
                       // Navigator.push(context,
                       //     MaterialPageRoute(builder: (context) =>
                       //         PlayAudioOnDemandScreen2(entryHelper.episodes.elementAt(index), _passedEntry.thumbNailUrl, _passedEntry.title)))
                       //     .then((value) {
                       //   if (value == 'home') {
                       //     Navigator.of(context).pop('home');
                       //   }
                       // });
                      },
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Row(
                          children: [
                            Column(
                              children: [
                                Container(
                                  width: MediaQuery.of(context).size.width * 0.8,
                                  child: Text(entryHelper.episodes.elementAt(index).episodeName, style: TextStyle(color: c.display.colors.bookRowTextColor),)
                                ),
                                Container(
                                    width: MediaQuery.of(context).size.width * 0.8,
                                    child: Text(entryHelper.episodes.elementAt(index).getLength(), style: TextStyle(color: c.display.colors.bookRowTextColor),)
                                ),
                              ],
                            ),
                           // Spacer(),
                            //Icon(FontAwesome.volume_up, size: 35.0, color: c.display.colors.bookRowTextColor),
                            //Icon(Icons.preview_outlined, size: 35.0, color: c.display.colors.bookRowTextColor),
                          ],
                        ),
                      )),
                ); //end of container
              },
                childCount: entryHelper.episodes.length,
              ), //end of sliver child buider delegate
            ), //end of sliver list
          ], //end of slivers
        ),
      ), //end of custom scroll view
    );
  }
}
