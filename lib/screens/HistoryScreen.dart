import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:audiobooks/popups/EmptyHistoryPopup.dart';
import 'package:audiobooks/popups/AddConfirmationPopup.dart';
import 'package:audiobooks/popups/BookInLibraryPopup.dart';
import 'package:audiobooks/objects/Constants.dart';
import 'package:audiobooks/objects/Library.dart';
import 'package:audiobooks/modules/EntryDetailHelper.dart';
import 'package:audiobooks/modules/HistoryFile.dart';
import 'package:audiobooks/modules/LocalFileHelper.dart';
import 'package:audiobooks/popups/DisplayBookInfoPopup.dart';
import 'package:audiobooks/popups/RemoveHistoryConfirmationPopup.dart';


class HistoryScreen extends StatefulWidget {
  @override
  _HistoryScreenState createState() => _HistoryScreenState();
}

class _HistoryScreenState extends State<HistoryScreen> {

  Library _library;
  HistoryFile _history;
  var c = Constants();

  _HistoryScreenState() {
    initLibrary();
  }

  void initLibrary() {
    _history = HistoryFile();
    _history.loadHistory().then((rcHistory) {
      if (_history.history.length == 0) {
        var rv1 = Navigator.of(context).push(PageRouteBuilder(
            opaque: false,
            pageBuilder: (BuildContext context, _, __) {
              return EmptyHistoryPopup();
            }
        )).then((value) => Navigator.of(context).pop());
      }
      setState(() {
        _history.history;
      });
    });
  }

  Widget _getPopupMenuForRow(int index) {
    return Padding(
      padding: const EdgeInsets.all(0.0),
      child: PopupMenuButton(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(9.0),
              side: BorderSide(color: c.display.colors.buttonBorderColor)
          ),
          icon: Icon(
              Icons.more_horiz_sharp,
              color: c.display.colors.bookRowTextColor
          ),
          itemBuilder: (context){
            return [
              PopupMenuItem<int>(
                value: 0,
                child: Row (
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Text("Info"),
                    Padding(
                      padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                      child: Icon(Icons.info_outline),
                    ),
                  ],
                ),
              ),
              PopupMenuItem<int>(
                value: 1,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Text("Restore to Library"),
                    Padding(
                      padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                      child: Icon(Icons.add_circle),
                    ),
                  ],
                ),
              ),
              PopupMenuItem<int>(
                value: 2,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Text("Remove From History"),
                    Padding(
                      padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                      child: Icon(Icons.delete),
                    ),
                  ],
                ),
              ),
            ];
          },
          onSelected:(value){
            if(value == 0){
              var rv = Navigator.of(context).push(PageRouteBuilder(
                  opaque: false,
                  pageBuilder: (BuildContext context, _, __) {
                    return DisplayBookInfoPopup(_history.history.elementAt(index).title, _history.history.elementAt(index).description);
                  }
              ));
            }else if(value == 1){
              var lh = LocalFileHelper();
              lh.isBookInLibrary(_history.history.elementAt(index).identifier).then((bookIsInLibrary) {
                if (bookIsInLibrary == true) {
                  var rv = Navigator.of(context).push(PageRouteBuilder(
                      opaque: false,
                      pageBuilder: (BuildContext context, _, __) {
                        return BookInLibraryPopup();
                      }
                  ));
                } else {
                  lh.writeDustJacketFileFromHistory(_history.history.elementAt(index));
                  var edh = EntryDetailHelper.forHistory(_history.history.elementAt(index).identifier);
                  edh.getEpisodes().then((value) {
                    lh.writeEpisodesFile(edh.episodes).then((value2) {
                      var rv = Navigator.of(context).push(PageRouteBuilder(
                          opaque: false,
                          pageBuilder: (BuildContext context, _, __) {
                            return AddConfirmationPopup();
                          }
                      ));
                    });
                  });
                }
              });
            } else if (value == 2) {
              var rv1 = Navigator.of(context).push(PageRouteBuilder(
                  opaque: false,
                  pageBuilder: (BuildContext context, _, __) {
                    return RemoveHistoryConfirmationPopup();
                  }
              )).then((value) {
                if (value == true) {
                  _history.removeItem(index).then((value) {
                    initLibrary();
                    setState(() {
                      _history.history;
                    });
                  });
                }
              });
            }
          }
      ),
    );
  }


  Widget _getListTileItem(int index) {
    return Container(
      decoration: BoxDecoration(
        border: Border(bottom: BorderSide(color: c.display.colors.bookRowTextColor)),
        color: c.display.colors.bookRowColor,
      ),
      child: Container(

        child: Row(
          children: [
            //*** image *****************
            Container(
              width: MediaQuery.of(context).size.width * 0.25,
              height: MediaQuery.of(context).size.width * 0.25,
              child: CachedNetworkImage(
                imageBuilder: (context, imageProvider) => Container(
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: imageProvider,
                      fit: BoxFit.fitWidth,
                      alignment: Alignment.centerLeft,
                    ),
                  ),
                ),
                imageUrl: _history.history.elementAt(index).imageUrl,  //_library.books.elementAt(index).dustJacket.imageUrl,
                placeholder: (context, url) {return Image.asset('assets/loading.gif');},
              ),
            ),
            //*** main text ***********************
            Expanded(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(_history.history.elementAt(index).title,    //_library.books.elementAt(index).dustJacket.title,          //tsh.titleList.elementAt(index).title,
                        style: TextStyle(fontWeight: FontWeight.bold, color: c.display.colors.bookRowTextColor)
                    ),
                    Text(_history.history.elementAt(index).author,     //_library.books.elementAt(index).dustJacket.author,     //tsh.titleList.elementAt(index).author,
                        style: TextStyle(fontWeight: FontWeight.normal, color: c.display.colors.bookRowTextColor)
                    ),
                    Text("Added: " + _history.history.elementAt(index).addDate,     //_library.books.elementAt(index).dustJacket.author,     //tsh.titleList.elementAt(index).author,
                        style: TextStyle(fontWeight: FontWeight.normal, color: c.display.colors.bookRowTextColor)
                    ),
                  ],
                ),
              ),
            ),
            Container(
              child: Column(
                children: [
                  Container(
                    child: _getPopupMenuForRow(index)
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: [
          IconButton(
              icon: Icon(Icons.home),
              onPressed: () {
                Navigator.of(context).pop('home');
              }),
        ],
        title: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(c.display.title, style: c.display.textStyleHeaderRegular),
                //c.display.jcusWebsite,
              ],
            ),
          ),
        ),
        toolbarHeight: MediaQuery.of(context).size.height * c.display.appBarHeight,
      ),

      body: Container(
        decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [
                c.display.colors.backgroundColor1,
                c.display.colors.backgroundColor2,
                c.display.colors.backgroundColor1,
                c.display.colors.backgroundColor2
              ],
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
            )
        ),
        child: Center(
          child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Expanded(
                  child: Container(
                    child: ListView.builder(
                      shrinkWrap: true,
                      itemCount: _history.history.length,  //_library.books.length,
                      itemBuilder: (BuildContext context,int index){
                        return _getListTileItem(index);
                      } ,
                    ),
                  ),
                ),
              ]
          ),
        ),
      ),
    );
  }
}



