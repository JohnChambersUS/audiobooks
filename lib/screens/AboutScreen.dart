
import 'package:flutter/material.dart';
import 'package:audiobooks/objects/Constants.dart';
import 'package:url_launcher/url_launcher.dart';

class AboutScreen extends StatefulWidget {
  @override
  _AboutScreenState createState() => _AboutScreenState();
}

class _AboutScreenState extends State<AboutScreen> {

  var c = Constants();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: [
          IconButton(
            icon: Icon(Icons.home),
              onPressed: () {
                Navigator.of(context).pop('home');
              }),
        ],
        title: Center(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(c.display.title, style: c.display.textStyleHeaderRegular),
                //c.display.jcusWebsite
              ],
            ),
          ),
        ),
        toolbarHeight: MediaQuery.of(context).size.height * c.display.appBarHeight,
      ),

      body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage(c.backgroundImages.elementAt(0)),
              fit: BoxFit.fitWidth,
              alignment: Alignment.bottomCenter,
            ),
            gradient: LinearGradient(
              colors: [
                c.display.colors.backgroundColor1,
                c.display.colors.backgroundColor2,
                c.display.colors.backgroundColor1,
                c.display.colors.backgroundColor2
              ],
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
            ),
          ),
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[

                Center(child: Text("JC4 Librivox",style: TextStyle(color: c.display.colors.inputBackgroundColor))),
                Center(child: Text("Copyright 2021 & 2022 John Chambers", style: TextStyle(color: c.display.colors.inputBackgroundColor))),
                Center(child: Text("All rights reserved", style: TextStyle(color: c.display.colors.inputBackgroundColor))),
                Center(child: Text("Version 3.0", style: TextStyle(color: c.display.colors.inputBackgroundColor))),

                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(),
                ),
                Text("This app was created as a thank you to the Librivox project.", style: TextStyle(color: c.display.colors.inputBackgroundColor)),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(),
                ),
                Text("It is ad free and provided at no charge. If you find someone charging for this app, please ask them to stop."
                    , style: TextStyle(color: c.display.colors.inputBackgroundColor)),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(),
                ),
                Text("Artwork by thejakesmith on Pixaby and used under the Simplified Pixaby License."
                    , style: TextStyle(color: c.display.colors.inputBackgroundColor)),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(),
                ),
                //Text("For documentation and info visit: ", style: TextStyle(color: c.display.colors.inputBackgroundColor)),
                c.display.jcusWebsiteAbout,
              ],
            ),
          ),
        ),
    );
  }

}
