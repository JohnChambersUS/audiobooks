import 'dart:async';
import 'package:audiobooks/objects/EpisodeEntry.dart';
import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:audiobooks/objects/Constants.dart';
import 'package:assets_audio_player/assets_audio_player.dart';

class PlayAudioOnDemandScreen2 extends StatefulWidget {

  EpisodeEntry _episode;
  String _imageUrl;
  String _bookName;

  PlayAudioOnDemandScreen2(EpisodeEntry episode, String imageUrl, bookName) {
    _episode = episode;
    _imageUrl = imageUrl;
    _bookName = bookName;
  }

  @override
  _PlayAudioOnDemandScreen2State createState() => _PlayAudioOnDemandScreen2State(_episode, _imageUrl, _bookName);
}

//*****************************************
//* second class
//*****************************************
class _PlayAudioOnDemandScreen2State extends State<PlayAudioOnDemandScreen2> {

  AssetsAudioPlayer _assetsAudioPlayer = AssetsAudioPlayer();
  final List<StreamSubscription> _subscriptions = [];

  EpisodeEntry _episode;
  String _imageUrl;
  String _bookName;

  String _currentPosition = "0";
  int _currentPositionInt = 0;

  String _duration = "0";
  int _durationInt = 1;

  Duration _playPoint = Duration();

  var _resumeOffstage = true;
  var _pauseOffstage = false;

  var c = Constants();

  var exitState = '';

  _PlayAudioOnDemandScreen2State(EpisodeEntry episode, String imageUrl, String bookName) {
    _imageUrl = imageUrl;
    _episode = episode;
    _bookName = bookName;
    play();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        stopAndExit();
      },
      child: Scaffold(
        appBar: AppBar(
          actions: [
            IconButton(
                icon: Icon(Icons.home),
                onPressed: () {
                  exitState = 'home';
                  stopAndExit();
                }),
          ],
          title: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(trimTitle(c.display.title), style: c.display.textStyleHeaderRegular),
                  //c.display.jcusWebsite,
                ],
              ),
            ),
          ),
          toolbarHeight: MediaQuery.of(context).size.height * c.display.appBarHeight,
        ),
        body: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                colors: [
                  c.display.colors.backgroundColor1,
                  c.display.colors.backgroundColor2,
                  c.display.colors.backgroundColor1,
                  c.display.colors.backgroundColor2
                ],
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
              )

          ),
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[

                Container(
                  height: MediaQuery.of(context).size.height * 0.30,
                  child: Center(
                    child: Padding(
                      padding: const EdgeInsets.only(left: 0.0, right: 0.0, bottom:0.0, top: 0.0),
                      child:  CachedNetworkImage(
                        imageBuilder: (context, imageProvider) => Container(
                          decoration: BoxDecoration(
                            image: DecorationImage(
                              image: imageProvider,
                              fit: BoxFit.fitHeight,
                            ),
                          ),
                        ),
                        imageUrl: _imageUrl,
                        placeholder: (context, url) {return Image.asset('assets/loading.gif');},
                      ),
                    ),
                  ),
                ),

                Spacer(),

                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Center(
                    child: Text("Previewing:\n" + _episode.episodeName,
                        style: TextStyle(fontWeight: FontWeight.bold, fontSize: 25,
                            color: c.display.colors.playerTitleColor
                        )
                    ),
                  ),
                ),

                Spacer(),

                //***
                //*** 2nd row of play controls
                //***
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [

                    FlatButton(
                      child: Icon(Icons.replay_30_rounded,
                        size:  MediaQuery.of(context).size.width * 0.16,
                        color: c.display.colors.playerTitleColor,
                      ),
                      shape: CircleBorder(),
                      onPressed: () {
                        rewind30Seconds();
                      },
                    ),

                    Offstage(
                      offstage: _pauseOffstage,
                      child: FlatButton(
                        child: Icon(Icons.pause_circle_filled_rounded,
                          size:  MediaQuery.of(context).size.width * 0.25,
                          color: c.display.colors.playerTitleColor,
                        ),
                        shape: CircleBorder(),
                        onPressed: () {
                          pause();
                        },
                      ),
                    ),

                    Offstage(
                      offstage: _resumeOffstage,
                      child: FlatButton(
                        child: Icon(Icons.play_circle_filled_rounded,
                          size:  MediaQuery.of(context).size.width * 0.25,
                          color: c.display.colors.playerTitleColor,
                        ),
                        shape: CircleBorder(),
                        onPressed: () {
                          resume();
                        },
                      ),
                    ), //end of flatbutton

                    FlatButton(
                      child: Icon(Icons.forward_30_rounded,
                        size:  MediaQuery.of(context).size.width * 0.16,
                        color: c.display.colors.playerTitleColor,
                      ),
                      shape: CircleBorder(),
                      onPressed: () {
                        fastForward30Seconds();
                      },
                    ),

                  ],
                ),
                //*** slider **************
                Padding(
                  padding: const EdgeInsets.only(bottom: 0.0),
                  child: Container(
                    width:  MediaQuery.of(context).size.width,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        Slider(
                          activeColor: c.display.colors.playerTitleColor,
                          inactiveColor: c.display.colors.sliderInactiveColor,
                          value: _currentPositionInt.toDouble(),
                          min: 0,
                          max: _durationInt.toDouble(),
                          divisions: _durationInt,
                          onChanged: (double value) {
                            setState(() {
                              _currentPositionInt = value.toInt();
                              var durr = Duration(seconds: value.toInt());
                              _assetsAudioPlayer.seek(durr); //todo audioPlayer.seek(durr);
                            });
                          },
                        ), // end of slider
                        Padding(
                          padding: EdgeInsets.only(left: 25.0, right: 25.0, bottom: 0.0, top: 0.0),
                          child: Row(
                            children: [
                              Text(_currentPosition.toString(),
                                  style: TextStyle(color: c.display.colors.playerTitleColor)
                              ),
                              Spacer(),
                              Text(_duration.toString(),
                                  style: TextStyle(color: c.display.colors.playerTitleColor)
                              ),

                            ],
                          ),
                        ),

                      ],
                    ),
                  ),
                ),
                Spacer(),

                Spacer(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  //****************************************
  //* play
  //****************************************
  play() async {
    var audios = <Audio>[
      Audio.network(_episode.getUrl(),
        metas: Metas(
          title: _episode.getEpisodeTitle(),
          artist: _bookName,
          image: MetasImage.network(_imageUrl),
        ),
      ),
    ];
    initSubscriptions();
    await _assetsAudioPlayer.open(
      Playlist(audios: audios, startIndex: 0),
      showNotification: true,
      notificationSettings: NotificationSettings(
          stopEnabled: false,
          customPrevAction: (player) {
            rewind30Seconds();
          },
          customNextAction: (player) {
            fastForward30Seconds();
          },
      ),
      autoStart: true,
    );
  }

  //******************************************
  //* rewinds player 30 seconds
  //******************************************
  rewind30Seconds() {
    var ppis = _playPoint.inSeconds - 30;
    if (ppis < 0) {ppis = 0;}
    Duration d = Duration(seconds: ppis);
    _assetsAudioPlayer.seek(d);
  }

  //******************************************
  //* fast forward player 30 seconds
  //******************************************
  fastForward30Seconds() {
    var ppis = _playPoint.inSeconds + 30;
    if (ppis > _durationInt) {ppis = _durationInt;};
    Duration d = Duration(seconds: ppis);
    _assetsAudioPlayer.seek(d);
  }

  //*****************************************
  //* pause playback
  //*****************************************
  pause() async {
    _assetsAudioPlayer.pause();
  }

  //******************************************
  //* resume playback
  //******************************************
  resume() async {
    _assetsAudioPlayer.play();
  }

  //*******************************************
  //* Set listeners for assets audio player
  //*******************************************
  initSubscriptions() {

    //current song info including duration
    _subscriptions.add(_assetsAudioPlayer.currentPosition.listen((data) {
      setDurationOnSlider(_assetsAudioPlayer.current.value.audio.duration);
      setCurrentPositionOnSlider(data);
    }));

    //current song info including duration
    _subscriptions.add(_assetsAudioPlayer.isPlaying.listen((data) {
      if (data == false) {
        setState(() {
          _pauseOffstage = true;
          _resumeOffstage = false;
        });
      } else {
        setState(() {
          _pauseOffstage = false;
          _resumeOffstage = true;
        });
      }
    }));

  }

  //****************************************************
  //* set current position on slider assets audio
  //****************************************************
  setCurrentPositionOnSlider(Duration duration) {
    setState(() {
      _currentPositionInt = duration.inSeconds;
      _playPoint = duration;
      String twoDigits(int n) => n.toString().padLeft(2, "0");
      String twoDigitSeconds = twoDigits(duration.inSeconds.remainder(60));
      if (duration.inHours > 0) {
        _currentPosition = "${duration.inHours}:${twoDigits(duration.inMinutes.remainder(60))}:$twoDigitSeconds";
      } else {
        _currentPosition = "${duration.inMinutes.remainder(60)}:$twoDigitSeconds";
      }
    });
  }

  //****************************************************
  //* setting duration on slider assets_audio version
  //****************************************************
  setDurationOnSlider(Duration duration) {
    setState(() {
      _durationInt = duration.inSeconds;
      String twoDigits(int n) => n.toString().padLeft(2, "0");
      var hours = "";
      if (duration.inHours > 0) {
        hours = duration.inHours.toString() + ":";
      }

      var minutes = "";
      if (duration.inHours > 0) {
        minutes = twoDigits(duration.inMinutes.remainder(60)) + ":";
      } else {
        minutes = duration.inMinutes.remainder(60).toString() + ":";
      }

      var seconds = "";
      if (duration.inMinutes.remainder(60) > 0) {
        seconds = twoDigits(duration.inSeconds.remainder(60));
      } else {
        seconds = duration.inSeconds.remainder(60).toString();
      }

      _duration = hours + minutes + seconds;

    });
  }

  //******************************************
  //* trim title
  //******************************************
  String trimTitle(String title) {
    var newTitle = "";
    var parts = title.split(" ");
    parts.forEach((element) {
      if (newTitle.length < 76) newTitle += element + " ";
    });
    return newTitle.trim();
  }

  //*****************************************
  //* stop & release player
  //*****************************************
  stopAndExit() {
    _assetsAudioPlayer.stop();
    _assetsAudioPlayer.dispose();
    Navigator.of(context).pop(exitState);
  }

} //end of second class
