
import 'package:audiobooks/screens/SearchChoicesScreen.dart';
import 'package:audiobooks/screens/LibraryScreen.dart';
import 'package:audiobooks/screens/HistoryScreen.dart';
import 'package:audiobooks/screens/AboutScreen.dart';
import 'package:flutter/material.dart';
import 'package:audiobooks/objects/Constants.dart';
import 'package:flutter/services.dart' ;
import 'package:audiobooks/modules/HistoryFile.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  var c = Constants();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'JC4 Librivox',
      theme: ThemeData(
        primarySwatch: c.display.colors.appBarColor,
      ),
      home: MyHomePage(title: 'JC4 Librovox'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  var c = Constants();

  @override
  Widget build(BuildContext context) {

    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);

    HistoryFile historyFile = HistoryFile();
    historyFile.verify(); //make sure history exists on first app run

    return Scaffold(
      appBar: AppBar(
        leading: Container(width: 50, color: Colors.transparent,),
        title: Center(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(c.display.title, style: c.display.textStyleHeaderRegular),
                //Text(c.display.subTitle, style: c.display.textStyleHeaderSmall),
                //c.display.jcusWebsite,
              ],
            ),
          ),
        ),
        actions: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: PopupMenuButton(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(9.0),
                  side: BorderSide(color: c.display.colors.buttonBorderColor)
                ),
                //icon: Icon(Icons.menu), //default is 3 dots
                itemBuilder: (context){
                  return [
                    PopupMenuItem<int>(
                      value: 0,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Text("History"),
                          Padding(
                            padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                            child: Icon(Icons.history, color: Colors.black87),
                          ),
                        ],
                      ),
                    ),
                    PopupMenuItem<int>(
                      value: 1,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Text("About"),
                          Padding(
                            padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                            child: Icon(Icons.info_outline, color: Colors.black87),
                          ),
                        ],
                      ),
                    ),
                  ];
                },
                onSelected:(value){
                  if(value == 0){
                    Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => HistoryScreen())
                    );
                  }else if(value == 1){
                    Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => AboutScreen())
                    );
                  }
                }
            ),
          ),
        ],
        toolbarHeight: MediaQuery.of(context).size.height * c.display.appBarHeight,
      ),

      body: Container(
        decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage(c.backgroundImages.elementAt(0)),
              fit: BoxFit.fitWidth,
              alignment: Alignment.bottomCenter,
            ),
            gradient: LinearGradient(
              colors: [
                c.display.colors.mainBackgroundColor1,
                c.display.colors.mainBackgroundColor2,
                c.display.colors.mainBackgroundColor1,
                c.display.colors.mainBackgroundColor2
              ],
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
            )
        ),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[

              Align (
                alignment: Alignment.topRight,
                child: TextButton(
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => SearchChoicesScreen())
                    );
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(top: 8, right: 8, bottom: 8),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Text("Search",
                          style: TextStyle(
                          color: c.display.colors.mainPageMenuColor,
                          fontSize: 24,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 16, top: 8, right: 8, bottom: 8),
                        child: Icon(
                            Icons.search,
                          color: c.display.colors.mainPageMenuColor,
                        ),
                      ),
                    ]),
                  ),
                ),
              ),

              Align (
                alignment: Alignment.topRight,
                child: TextButton(
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => LibraryScreen())
                    );
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(top: 0, right: 8, bottom: 8),
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Text("Library",
                            style: TextStyle(
                              color: c.display.colors.mainPageMenuColor,
                              fontSize: 24,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 16, top: 0, right: 8, bottom: 0),
                            child: Icon(
                              Icons.local_library_outlined,
                              color: c.display.colors.mainPageMenuColor,
                            ),
                          ),
                        ]
                    ),
                  ),
                ),
              ),
            ],
          ), //end of column container
        ), //end of center container
      ), //end of body container
    );
  }
}
