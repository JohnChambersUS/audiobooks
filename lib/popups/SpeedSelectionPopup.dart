import 'dart:ui';
import 'package:flutter/material.dart';

class SpeedSelectionPopup extends StatefulWidget {

  double _speed;
  SpeedSelectionPopup(this._speed);

  @override
  _SpeedSelectionPopupState createState() => _SpeedSelectionPopupState(_speed);
}

class _SpeedSelectionPopupState extends State<SpeedSelectionPopup> {

  double _speed;
  TextStyle textStyleHeader = TextStyle (color: Colors.black, fontSize: 25.0, fontWeight: FontWeight.bold);
  TextStyle textStyleBody = TextStyle (color: Colors.black, fontSize: 15.0, fontWeight: FontWeight.normal);

  _SpeedSelectionPopupState(this._speed);

  @override
  Widget build(BuildContext context) {

    return Center(
      child: Padding(
          padding: const EdgeInsets.all(0.0),
          child: AlertDialog(

            title: Text("Playback Speed",style: textStyleHeader,),
            content: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              //crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                IconButton(
                  icon: Icon(Icons.arrow_back_ios),
                  onPressed: () {
                    decrementSpeed();
                  },
                ),
                Spacer(),
                Text(_speed.toString() + "x"),
                Spacer(),
                IconButton(
                  icon: Icon(Icons.arrow_forward_ios),
                  onPressed: () {
                    incrementSpeed();
                  },
                ),

              ],
            ),

            actions: <Widget>[
              FlatButton(
                child: Text("OK"),
                onPressed: () {
                  Navigator.of(context).pop(_speed);
                },
              ),
            ],
          )
      ),
    );

  }

  String decrementSpeed() {
    _speed -= 0.1;
    if (_speed < 0.5) {_speed = 0.5;}
    _speed = double.parse(_speed.toStringAsFixed(2));
    setState(() {
      _speed;
    });
  }

  String incrementSpeed() {
    _speed += 0.1;
    if (_speed >= 2.1) {_speed = 2.0;}
    _speed = double.parse(_speed.toStringAsFixed(2));
    setState(() {
      _speed;
    });
  }

}
