
import 'dart:ui';
import 'package:flutter/material.dart';


class ServiceIsDownPopup extends StatelessWidget {

  String title;
  String content;

  ServiceIsDownPopup();
  TextStyle textStyleHeader = TextStyle (color: Colors.black, fontSize: 25.0, fontWeight: FontWeight.bold);
  TextStyle textStyleBody = TextStyle (color: Colors.black, fontSize: 15.0, fontWeight: FontWeight.normal);

  @override
  Widget build(BuildContext context) {

    return Center(
      child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: AlertDialog(
            title: Text("Unable to Perform Search",style: textStyleHeader,),
            content: Text("The book service is returning a 503 which indicates the service is down at the moment. Please try again later.",
              style: textStyleBody,),
            actions: <Widget>[
              FlatButton(
                child: Text("OK"),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          )
      ),
    );

  }
}
