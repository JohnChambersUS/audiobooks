
import 'dart:ui';
import 'package:flutter/material.dart';


class BookCompletePopupPlayAll extends StatelessWidget {

  String title;
  String content;

  BookCompletePopupPlayAll();
  TextStyle textStyleHeader = TextStyle (color: Colors.black, fontSize: 25.0, fontWeight: FontWeight.bold);
  TextStyle textStyleBody = TextStyle (color: Colors.black, fontSize: 15.0, fontWeight: FontWeight.normal);

  @override
  Widget build(BuildContext context) {

    return Center(
      child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: AlertDialog(
            title: Text("Book is complete",style: textStyleHeader,),
            content: Text("Last chapter of book has played. Tap reset then tap play to replay the book from the beginning.",
              style: textStyleBody,),
            actions: <Widget>[
              FlatButton(
                child: Text("OK"),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          )
      ),
    );

  }
}
