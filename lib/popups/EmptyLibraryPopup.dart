
import 'dart:ui';
import 'package:flutter/material.dart';


class EmptyLibraryPopup extends StatelessWidget {

  String title;
  String content;

  EmptyLibraryPopup();
  TextStyle textStyleHeader = TextStyle (color: Colors.black, fontSize: 25.0, fontWeight: FontWeight.bold);
  TextStyle textStyleBody = TextStyle (color: Colors.black, fontSize: 15.0, fontWeight: FontWeight.normal);

  @override
  Widget build(BuildContext context) {

    return Center(
      child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: AlertDialog(
            title: Text("No Library Books Found",style: textStyleHeader,),
            content: Text("There are no books in your library. Perform a search and add a book, then come here to listen to it.",
              style: textStyleBody,),
            actions: <Widget>[
              FlatButton(
                child: Text("OK"),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          )
      ),
    );

  }
}
