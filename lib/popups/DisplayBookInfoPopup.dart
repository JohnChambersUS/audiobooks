import 'dart:ui';
import 'package:flutter/material.dart';

class DisplayBookInfoPopup extends StatelessWidget {

  String title;
  String content;

  DisplayBookInfoPopup(this.title, this.content);

  TextStyle textStyleHeader = TextStyle (color: Colors.black, fontSize: 25.0, fontWeight: FontWeight.bold);
  TextStyle textStyleBody = TextStyle (color: Colors.black, fontSize: 15.0, fontWeight: FontWeight.normal);

  @override
  Widget build(BuildContext context) {

    return Center(
      child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: AlertDialog(
            title: Text(title, style: textStyleHeader),
            content: SingleChildScrollView(
              child: Text(content,
                style: textStyleBody,),
            ),
            actions: <Widget>[
              FlatButton(
                child: Text("OK"),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          )
      ),
    );

  }
}
