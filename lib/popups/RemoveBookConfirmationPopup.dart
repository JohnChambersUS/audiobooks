
import 'dart:ui';
import 'package:flutter/material.dart';

class RemoveBookConfirmationPopup extends StatelessWidget {

  String title;
  String content;

  RemoveBookConfirmationPopup();
  TextStyle textStyleHeader = TextStyle (color: Colors.black, fontSize: 25.0, fontWeight: FontWeight.bold);
  TextStyle textStyleBody = TextStyle (color: Colors.black, fontSize: 15.0, fontWeight: FontWeight.normal);

  @override
  Widget build(BuildContext context) {

    return Center(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: AlertDialog(
          //title: Text("Book is complete",style: textStyleHeader,),
          content: Text("Do you really want to remove this book?",
            style: textStyleBody,),
          actions: <Widget>[
            FlatButton(
              child: Text("Yes"),
              onPressed: () {
                Navigator.of(context).pop(true);
              },
            ),
            FlatButton(
              child: Text("No"),
              onPressed: () {
                Navigator.of(context).pop(false);
              },
            ),
          ],
        )
      ),
    );

  }
}
