import 'dart:ui';
import 'package:flutter/material.dart';

class SleepTimerPopup extends StatefulWidget {

  SleepTimerPopup();

  @override
  _SleepTimerPopupState createState() => _SleepTimerPopupState();
}

class _SleepTimerPopupState extends State<SleepTimerPopup> {

  int _minutes = -1;
  TextStyle textStyleHeader = TextStyle (color: Colors.black, fontSize: 25.0, fontWeight: FontWeight.bold);
  TextStyle textStyleBody = TextStyle (color: Colors.black, fontSize: 15.0, fontWeight: FontWeight.normal);

  _SleepTimerPopupState();

  @override
  Widget build(BuildContext context) {

    return Center(
      child: Padding(
          padding: const EdgeInsets.all(0.0),
          child: AlertDialog(

            title: Text("Sleep Timer",style: textStyleHeader,),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                RadioListTile(
                  title: Text("20 minutes"),
                  value: 20,
                  groupValue: _minutes,
                  onChanged: (value){
                    _minutes = 20;
                    setState(() {
                      _minutes;
                    });
                  },
                ),
                RadioListTile(
                  title: Text("40 minutes"),
                  value: 40,
                  groupValue: _minutes,
                  onChanged: (value){
                    _minutes = 40;
                    setState(() {
                      _minutes;
                    });
                  },
                ),
                RadioListTile(
                  title: Text("1 hour"),
                  value: 60,
                  groupValue: _minutes,
                  onChanged: (value){
                    _minutes = 60;
                    setState(() {
                      _minutes;
                    });
                  },
                ),
                RadioListTile(
                  title: Text("2 hours"),
                  value: 120,
                  groupValue: _minutes,
                  onChanged: (value){
                    _minutes = 120;
                    setState(() {
                      _minutes;
                    });
                  },
                ),
                RadioListTile(
                  title: Text("Unlimited"),
                  value: -1,
                  groupValue: _minutes,
                  onChanged: (value){
                    _minutes = -1;
                    setState(() {
                      _minutes;
                    });
                  },
                )
              ]
            ),
            actions: <Widget>[
              //Spacer(),
              FlatButton(
                child: Text("OK"),
                onPressed: () {
                  Navigator.of(context).pop(_minutes);
                },
              ),
            ],
          )
      ),
    );

  }

}
