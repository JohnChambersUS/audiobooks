
import 'dart:ui';
import 'package:flutter/material.dart';


class EmptyHistoryPopup extends StatelessWidget {

  String title;
  String content;

  EmptyHistoryPopup();
  TextStyle textStyleHeader = TextStyle (color: Colors.black, fontSize: 25.0, fontWeight: FontWeight.bold);
  TextStyle textStyleBody = TextStyle (color: Colors.black, fontSize: 15.0, fontWeight: FontWeight.normal);

  @override
  Widget build(BuildContext context) {

    return Center(
      child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: AlertDialog(
            title: Text("History is Empty",style: textStyleHeader,),
            content: Text("There are no books in your History. When you add a book to your libary it will also be added to your history.",
              style: textStyleBody,),
            actions: <Widget>[
              FlatButton(
                child: Text("OK"),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          )
      ),
    );

  }
}
