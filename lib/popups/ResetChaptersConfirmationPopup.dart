import 'dart:ui';
import 'package:flutter/material.dart';

class ResetChaptersConfirmationPopup extends StatelessWidget {

  String title;
  String content;

  ResetChaptersConfirmationPopup();
  TextStyle textStyleHeader = TextStyle (color: Colors.black, fontSize: 25.0, fontWeight: FontWeight.bold);
  TextStyle textStyleBody = TextStyle (color: Colors.black, fontSize: 15.0, fontWeight: FontWeight.normal);

  @override
  Widget build(BuildContext context) {

    return Center(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: AlertDialog(
          content: Text("Do you really want to reset all chapters to the beginning?",
            style: textStyleBody,),
          actions: <Widget>[
            FlatButton(
              child: Text("Yes"),
              onPressed: () {
                Navigator.of(context).pop(true);
              },
            ),
            FlatButton(
              child: Text("No"),
              onPressed: () {
                Navigator.of(context).pop(false);
              },
            ),
          ],
        )
      ),
    );

  }
}
