
import 'dart:ui';
import 'package:flutter/material.dart';


class BookCompletePopup extends StatelessWidget {

  String title;
  String content;

  BookCompletePopup();
  TextStyle textStyleHeader = TextStyle (color: Colors.black, fontSize: 25.0, fontWeight: FontWeight.bold);
  TextStyle textStyleBody = TextStyle (color: Colors.black, fontSize: 15.0, fontWeight: FontWeight.normal);

  @override
  Widget build(BuildContext context) {

    return Center(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: AlertDialog(
          title: Text("Book is complete",style: textStyleHeader,),
          content: Text("Last chapter of book has played",
            style: textStyleBody,),
          actions: <Widget>[
            FlatButton(
              child: Text("OK"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        )
      ),
    );

  }
}
