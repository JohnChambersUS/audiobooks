import 'dart:collection';
import 'package:audiobooks/modules/UrlHelper.dart';
import 'package:audiobooks/objects/TitleSearchEntry.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class TitleSearchHelper {

  List<TitleSearchEntry> titleList = [];
  var urlHelper = UrlHelper();

  var _rows = 200;
  var _page = 0;

  var _archiveUrlSearchTitle = "https://archive.org/advancedsearch.php?output=json&fl[]=title,identifier,creator,description,avg_rating&q=collection:(librivoxaudio) AND mediatype:(audio) AND title:(##title##)&rows=##rows##&page=##page##";

  void clearTitleList() {
    titleList = [];
    _page = 0;
  }

  //***
  //*** search
  //***
  Future<bool> search(String title) async {
    _page++;
    var rawSearch = _archiveUrlSearchTitle;
    rawSearch = _archiveUrlSearchTitle.replaceFirst("##title##", title);
    rawSearch = rawSearch.replaceFirst("##rows##", _rows.toString());
    rawSearch = rawSearch.replaceFirst("##page##", _page.toString());
    var url = Uri.encodeFull(rawSearch);
    var response = await http.get(Uri.parse(url));

    var rc = response.statusCode;
    if (rc != 200) {
      return false;
    }
    var body = response.body;
    var j = json.decode(body);

    var r = j["response"] as LinkedHashMap;
    var books = r["docs"] as List<dynamic>;

    books.forEach((bookValue) {
      var bv = bookValue as LinkedHashMap;
      var currSearchEntry = TitleSearchEntry();
      bv.forEach((itemKey, itemValue) {
        if (itemKey == "identifier") {
          currSearchEntry.setId(itemValue.toString());
        }
        if (itemKey == "title") {
          currSearchEntry.setTitle(itemValue.toString());
        }
        if (itemKey == "creator") {
          var itemValueString = itemValue.toString();
          var authorName = formatAuthorString(itemValueString);
          currSearchEntry.addAuthor(authorName);
        }
        if (itemKey == "description") {
          currSearchEntry.description =  itemValue.toString();
        }
        if (itemKey == "avg_rating") {
          currSearchEntry.avgRating =  itemValue.toString();
        }
      });
      titleList.add(currSearchEntry);
    });

    return true;
  }

  //***
  //*** format author string
  //***
  String formatAuthorString(String authors) {
    var returnValue = authors;
   return returnValue;
  }

}