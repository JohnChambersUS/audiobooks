import 'dart:io';
import 'package:audiobooks/objects/EpisodeFromDb.dart';
import 'package:audiobooks/objects/TitleSearchEntry.dart';
import 'package:audiobooks/objects/HistoryRecord.dart';
import 'package:audiobooks/objects/EpisodeEntry.dart';
import 'package:path_provider/path_provider.dart';

class LocalFileHelper {

  var _s = ":©®:";
  var _n = "\n";
  var _t = "\t";
  var _dustJacketFileName = "##id##.dust_jacket.dbfile";
  var _episodesFileName = "##id##.table_of_contents.dbfile";

  //*********************************************
  //* write dust jacket file
  //*********************************************
  Future<bool> writeDustJacketFile(TitleSearchEntry titleEntry) async {

    var outputString = "";
    outputString += "title" + _s + titleEntry.title.trim() + _n;
    outputString += "author" + _s + titleEntry.author.trim() + _n;
    outputString += "image_url" + _s + titleEntry.imageUrl.trim() + _n;
    outputString += "description" + _s + titleEntry.description.trim() + _n;
    outputString += "identifier" + _s + titleEntry.id.trim() + _n;
    outputString += "avg_rating" + _s + titleEntry.avgRating.trim() + _n;
    var tt = await titleEntry.getTotalTime();
    outputString += "total_time" + _s + tt.toString().trim() + _n;
    outputString += "speed" + _s + "1.0";

    var saveFileName = _dustJacketFileName.replaceFirst("##id##",titleEntry.id.trim());

    final directory = await getApplicationDocumentsDirectory();
    var path = directory.path.toString() + "/" + saveFileName;
    var f = File(path);
    var out;
    try {
      out = await f.writeAsString(outputString);
    } catch (e) {
      return false;
    }
    return true;
  }

  //*********************************************
  //* write dust jacket file
  //*********************************************
  Future<bool> writeDustJacketFileFromHistory(HistoryRecord titleEntry) async {

    var outputString = "";
    outputString += "title" + _s + titleEntry.title.trim() + _n;
    outputString += "author" + _s + titleEntry.author.trim() + _n;
    outputString += "image_url" + _s + titleEntry.imageUrl.trim() + _n;
    outputString += "description" + _s + titleEntry.description.trim() + _n;
    outputString += "identifier" + _s + titleEntry.identifier.trim() + _n;
    outputString += "avg_rating" + _s + titleEntry.avgRating.toString().trim() + _n;
    outputString += "total_time" + _s + titleEntry.totalTime.trim() + _n;
    outputString += "speed" + _s + "1.0";

    var saveFileName = _dustJacketFileName.replaceFirst("##id##",titleEntry.identifier.trim());

    final directory = await getApplicationDocumentsDirectory();
    var path = directory.path.toString() + "/" + saveFileName;
    var f = File(path);
    var out;
    try {
      out = await f.writeAsString(outputString);
    } catch (e) {
      return false;
    }
    return true;
  }

  //*************************************
  //* write episodes file
  //*************************************
  Future<bool> writeEpisodesFile(List<EpisodeEntry> episodes) async {

    var outputString = "";
    var i = 1001;

    episodes.forEach((episode) {
      outputString += "sort" + _s + i.toString() + _t;
      i++;
      outputString += "play_point" + _s + "0" + _t;
      outputString += "duration" + _s + "1" + _t;
      outputString += "url" + _s + episode.getUrl() + _t;
      outputString += "title" + _s + episode.episodeName + _t;
      outputString += "length" + _s + episode.getLength();
      outputString += _n;
    });
    outputString = outputString.substring(0, outputString.length - 1);

    var saveFileName = _episodesFileName.replaceFirst("##id##",episodes.elementAt(0).getIdentity().trim());

    final directory = await getApplicationDocumentsDirectory();
    var path = directory.path.toString() + "/" + saveFileName;
    var f = File(path);
    var out;
    try {
      out = await f.writeAsString(outputString);
    } catch (e) {
      return false;
    }
    return true;
  }

  //***************************************
  //* get reading list
  //***************************************
  Future<List<TitleSearchEntry>> getReadingList() async {
    List<TitleSearchEntry> returnList = [];
    final directory = await getApplicationDocumentsDirectory();
    final files = await directory.list().toList();
    for (final fileName in files) {
      final parts = fileName.path.toString().split(".");
      final type = parts.last.trim();
      if (type == "dbfile") {
        if (parts.elementAt(parts.length -2) == "dust_jacket") {
          var file = File(fileName.path.toString());
          if (await file.exists()) {
            var contents = await file.readAsString();
            returnList.add(parseDustJacketFiles(contents));
          }
        }
      }
    }
    return returnList;
  }

  //***
  //*** parse dust jacket
  //***
  TitleSearchEntry parseDustJacketFiles(String contents) {
    var fileParts = contents.split(_n);
    var newEntry = TitleSearchEntry();
    fileParts.forEach((item) {
      var kv = item.split(_s);
      if (kv.length > 1) {
        switch (kv.elementAt(0).trim()) {
          case "title":
            {
              newEntry.title = kv.elementAt(1).trim();
            }
            break;
          case "author":
            {
              newEntry.author = kv.elementAt(1).trim();
            }
            break;
          case "image_url":
            {
              newEntry.imageUrl = kv.elementAt(1).trim();
            }
            break;
          case "description":
            {
              newEntry.description = kv.elementAt(1).trim();
            }
            break;
          case "identifier":
            {
              newEntry.id = kv.elementAt(1).trim();
            }
            break;
          case "avg_rating":
            {
              newEntry.avgRating = kv.elementAt(1).trim();
            }
            break;
          case "total_time":
            {
              newEntry.totalTime = kv.elementAt(1).trim();
            }
            break;
        }
      }
    });
    return newEntry;
  }

  Future<bool> isBookInLibrary(String id) async {
    var fileName = _dustJacketFileName.replaceFirst("##id##",id.trim());
    final directory = await getApplicationDocumentsDirectory();
    var path = directory.path.toString() + "/" + fileName;
    var f = await File(path);
    if(await f.exists() == true) {
      return Future.value(true);
    } else {
      return Future.value(false);
    }
  }

}