
import 'package:audiobooks/modules/ASearchBase.dart';

class ASearchTitle extends ASearchBase {

  var _searchBaseSection = "https://archive.org/advancedsearch.php?output=json&fl[]=title,identifier,creator,description,avg_rating&rows=##rows##&page=##page##";
  var _searchTitleSection = "&q=collection:(librivoxaudio) AND mediatype:(audio) AND title:(##title##)";
  var _searchCategorySection = " AND subject:(##category##)";

  //***
  //*** compose search url must be overridden
  //***
  @override
  String composeSearchUrl() {

    var rawSearch = _searchBaseSection.replaceFirst("##rows##", rows.toString());
    rawSearch = rawSearch.replaceFirst("##page##", page.toString());
    rawSearch += _searchTitleSection.replaceFirst("##title##", titleSearchString);
    if (categorySearchString.length > 0) {
      rawSearch += _searchCategorySection.replaceFirst("##category##", categorySearchString);
    }
    return rawSearch;

  }







}