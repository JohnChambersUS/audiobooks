
import 'package:audiobooks/objects/TitleSearchEntry.dart';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'dart:collection';
import 'dart:convert';

abstract class ASearchBase {

  List<TitleSearchEntry> titleList = [];
  @protected var page = 0;
  @protected final int rows = 100;

  @protected var titleSearchString = "";
  @protected var authorSearchString = "";

  @protected var positionSearchString = "";
  @protected var categorySearchString = "";

  @protected var narratorSearchString = "";

  @protected var searchOrderString = "";

  @protected var lastReturnCode = 0;



  ASearchBase() {
    if (categorySearchString == "any category") {
      categorySearchString = "";
    }
  }

  //***
  //*** set search order search
  //***
  void setSearchOrder(String order) {
    searchOrderString = order.toLowerCase().trim();
  }

  //***
  //*** set title to search
  //***
  void setTitleToSearch(String title) {
    titleSearchString = title.toLowerCase().trim();
  }

  //***
  //*** set author to search
  //***
  void setAuthorToSearch(String author) {
    authorSearchString = author.toLowerCase().trim();
  }

  //***
  //*** set author to search
  //***
  void setNarratorToSearch(String narrator) {
    narratorSearchString = narrator.toLowerCase().trim();
  }

  //***
  //*** get last return code from search
  //***
  int getLastReturnCode() {
    return lastReturnCode;
  }


  void setCategoryToSearch(String category) {
    categorySearchString = category.toLowerCase().trim();
    if (categorySearchString == "any category") {
      categorySearchString = "";
    }
  }

  //***
  //*** compose search url must be overridden
  //***
  String composeSearchUrl();

  //***
  //*** search
  //***
  Future<bool> search() async {
    page++;
    var rawSearch = composeSearchUrl();
    var url = Uri.encodeFull(rawSearch);

    var response = await http.get(Uri.parse(url));

    var rc = response.statusCode;
    lastReturnCode = rc;
    if (rc != 200) {
      return false;
    }
    var body = response.body;
    var j = json.decode(body);

    var r = j["response"] as LinkedHashMap;
    var books = r["docs"] as List<dynamic>;

    books.forEach((bookValue) {
      var bv = bookValue as LinkedHashMap;
      var currSearchEntry = TitleSearchEntry();
      bv.forEach((itemKey, itemValue) {
        if (itemKey == "identifier") {
          currSearchEntry.setId(itemValue.toString());
        }
        if (itemKey == "title") {
          currSearchEntry.setTitle(itemValue.toString());
        }
        if (itemKey == "creator") {
          currSearchEntry.author = itemValue.toString();
        }
        if (itemKey == "description") {
          currSearchEntry.description =  itemValue.toString();
        }
        if (itemKey == "avg_rating") {
          currSearchEntry.avgRating =  itemValue.toString();
        }
      });
      titleList.add(currSearchEntry);
    });

    return true;
  }

}
