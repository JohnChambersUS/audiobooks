
import 'package:audiobooks/modules/ASearchBase.dart';

class ASearchCategory extends ASearchBase {

  var _searchBaseSection = "https://archive.org/advancedsearch.php?sort[]=##order##&output=json&fl[]=title,identifier,creator,description,avg_rating&rows=##rows##&page=##page##";
  var _searchLimitSection = "&q=collection:(librivoxaudio) AND mediatype:(audio)";
  var _searchCategorySection = " AND subject:(##category##)";

  //***
  //*** compose search url must be overridden
  //***
  @override
  String composeSearchUrl() {

    var rawSearch = _searchBaseSection.replaceFirst("##rows##", rows.toString());
    rawSearch = rawSearch.replaceFirst("##page##", page.toString());

    switch(searchOrderString.toLowerCase()) {
      case "newest first": rawSearch = rawSearch.replaceFirst("##order##", "addeddate+desc");
        break;
      case "most popular": rawSearch = rawSearch.replaceFirst("##order##", "downloads+desc");
        break;
      case "oldest first": rawSearch = rawSearch.replaceFirst("##order##", "addeddate+asc");
        break;
    }

    rawSearch = rawSearch + _searchLimitSection;
    if (categorySearchString.length > 0) {
      rawSearch += _searchCategorySection.replaceFirst("##category##", categorySearchString);
    }
    return rawSearch;
  }

}