
class UrlHelper {


  var _archiveUrlGetImage = "https://archive.org/download/##id##/##filename##";
  var _archiveUrlSearchTitle = "https://archive.org/advancedsearch.php?output=json&fl[]=title,identifier,creator,description,avg_rating&q=collection:(librivoxaudio) AND mediatype:(audio) AND title:(##title##)";


  String getUrlSearchTitle(String title) {
    var r = _archiveUrlSearchTitle.replaceFirst("##title##", title);
    return r;
  }



}