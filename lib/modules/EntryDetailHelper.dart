import 'package:audiobooks/objects/EpisodeEntry.dart';
import '../objects/TitleSearchEntry.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:collection';

class EntryDetailHelper {

  TitleSearchEntry _entry;
  List<EpisodeEntry> episodes = [];
  var _speed128 = "128Kbps MP3";
  var _speed64 = "64Kbps MP3";


  EntryDetailHelper(TitleSearchEntry entry) {
    _entry = entry;
  }

  EntryDetailHelper.forHistory(String id) {
    _entry = TitleSearchEntry();
    _entry.id = id;
  }


  //***
  //*** get episodes
  //***
  Future<bool> getEpisodes() async {
    episodes.clear();
    var getFilesUrl = "https://archive.org/metadata/##id##/files";
    getFilesUrl = getFilesUrl.replaceFirst("##id##", _entry.id.toString());
    var response = await  http.get(Uri.parse(getFilesUrl));

    if (response.statusCode != 200) {
      return false;
    }

    var body = response.body;
    var j = json.decode(body);
    var result = j["result"] as List<dynamic>;

    parseEpisodes(result, _speed128);
    if (episodes.isEmpty) {
      parseEpisodes(result, _speed64);
    }
    return true;
  }

  void parseEpisodes( List<dynamic> result, String speed) {

    for ( var i = 0 ; i < result.length; i++ ) {
      var e = result.elementAt(i) as LinkedHashMap;
      if (e["format"].toString().toLowerCase() == speed.toLowerCase()) {
        var entry = EpisodeEntry();
        entry.setFileName(e["name"].toString());
        entry.episodeName = e["title"].toString();
        entry.setIdentity(_entry.id.toString());
        if (speed.toLowerCase() == _speed64.toLowerCase()) {
          entry.setLength(e["length"]);
        } else {
          try {
            var seconds = double.parse(e["length"]).truncate();
            var hours = (seconds / 3600).truncate();
            var afterHours = seconds % 3600;
            var mins = (afterHours / 60).truncate();
            var secs = afterHours % 60;

            var hoursP = "";
            if (hours > 0) {
              hoursP = hours.toString() + ":";
            }

            var minsP = "";
            if (mins > 0) {
              if ((mins < 10) && (hoursP.length > 0)) {
                minsP = "0" + mins.toString() + ":";
              } else {
                minsP = mins.toString() + ":";
              }
            }

            var secsP = "";
            if (secs > 9) {
              secsP = secs.toString();
            } else {
              secsP = "0" + secs.toString();
            }

            var time = hoursP + minsP + secsP;
            entry.setLength(time);
          } catch (e) {
            entry.setLength("");
          }
        }
        episodes.add(entry);
      }
    }
  }



}