import 'dart:io';
import 'package:path_provider/path_provider.dart';
import 'package:audiobooks/objects/TitleSearchEntry.dart';
import 'package:audiobooks/objects/HistoryRecord.dart';
import 'dart:developer';
import 'package:audiobooks/objects/Constants.dart';
import 'dart:core';

class HistoryFile {

  String _urlTemplate = "global.history.dbfile";

  var _n = "\n";
  var _t = "\t";

  List<HistoryRecord> history = [];
  String path = "";
  Constants c;

  HistoryFile() {
    c = Constants();
  }

  Future<bool> verify() async {
    var directory = await getApplicationDocumentsDirectory();
    c.applicationPath = directory.path.toString();
    var path = c.applicationPath + "/" + _urlTemplate;
    var file = await File(path);
    if (await file.exists() == true)  {
      return Future.value(true);
    } else {
      await file.create().then((value) {
        if (value != null) {
          if (value == true) {
            return Future.value(true);
          } else {
            return Future.value(false);
          }
        } else {
          //log("*** verify returned null");
        }
      });
    }
  }

  Future<bool> isEmpty() async {
    final directory = await getApplicationDocumentsDirectory();
    var path = directory.path.toString() + "/" + _urlTemplate;
    var empty = await File(path).length() == 0;
    if (empty) {
      return true;
    } else {
      return false;
    }
  }

  Future<bool> addToHistory(TitleSearchEntry titleEntry) async {
      var outputString = "";
      outputString += titleEntry.title.trim().replaceAll(_t, " ").replaceAll(_n, " ") + _t; //title
      outputString += titleEntry.author.trim().replaceAll(_t, " ").replaceAll(_n, " ") + _t; //author
      outputString += titleEntry.imageUrl.trim().replaceAll(_t, " ").replaceAll(_n, " ") + _t; //image_url
      outputString += titleEntry.description.trim().replaceAll(_t, " ").replaceAll(_n, " ") + _t; //description
      outputString += titleEntry.id.trim().replaceAll(_t, " ").replaceAll(_n, " ") + _t; //identifier
      String avgRating = titleEntry.avgRating.trim().replaceAll(_t, "").replaceAll(_n, ""); //avg_rating
      if (avgRating.length < 1) {avgRating = "0.0";}
      outputString += avgRating + _t;
      outputString += titleEntry.totalTime.trim().replaceAll(_t, " ").replaceAll(_n, " ") + _t; //total_time
      //add current time
      DateTime now = DateTime.now();
      String day = now.year.toString() + "-" + now.month.toString() + "-" + now.day.toString();
      outputString += day + _n; //time book was added

      await getApplicationDocumentsDirectory().then((dir) async {
        //log("*** addToHistory3 getting directory");
        var path = dir.path.toString() + "/" + _urlTemplate;
        var f = await File(path);
        var out = await f.writeAsString(outputString, mode: FileMode.append);
        return Future.value(true);
      });
    }

  Future<bool> loadHistory() async {
    var path = c.applicationPath + "/" + _urlTemplate;
    var f = File(path);
    List<String> lines = f.readAsLinesSync();
    for (var line in lines) {
      history.add(HistoryRecord(line));
    }
    return true;
  }

  Future<bool> removeItem(int index) async {
    if (history.length == 0) return Future.value(true);
    history.removeAt(index);
    var path = c.applicationPath + "/" + _urlTemplate;
    var f = await File(path);
    await f.writeAsStringSync('');
    for (var book in history) {
      var outLine = "";
      await f.writeAsString(book.convertToString(), mode: FileMode.append);
    }
    return Future.value(true);
  }

  Future<bool> isBookInHistory(String id) async {
    return await loadHistory().then((value) {
      for (var entry in history) {
        if (entry.identifier == id) {
          return true;
        }
      }
      return false;
    });
  }
}